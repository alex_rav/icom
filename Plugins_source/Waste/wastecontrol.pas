unit wastecontrol;
// ���������� waste

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  inifiles;

procedure rescan;
procedure connect;
procedure disconnect;
procedure Search(SR: string);
procedure transfer;

implementation

const
  wasteserv='172.16.23.32:53210   ';

procedure Delay(msecs : Longint);
var
   FirstTick : int64;
   i:integer;
begin
  i:=0;
  randomize;
  FirstTick:=GetTickCount;
  repeat
    i:=i+1;
    if i<=1000 then
       Application.ProcessMessages
    else
    begin
       //i:=0;
       break;
    end;
  until abs(GetTickCount)-abs(FirstTick) >= msecs;
end;

procedure mystrcopy(var dest:string;source:pchar);
var
 i:integer;
begin
 i:=0; setlength(dest,0);
 while true do
 begin
   if source[i]<>#0 then
   begin
      setlength(dest,i+1);
      dest[i+1]:=source[i];
   end
   else
      break;
   i:=i+1;
 end;
end;

procedure wini;
var
 fi:Tinifile;
 ss:string;
begin
 if wasteserv>'' then
 begin
    fi:=Tinifile.Create(extractfilepath(application.exename)+'default.pr0');
    ss:=fi.readString('profile','concb_0','');
    fi.WriteString('profile','concb_0',wasteserv);
    fi.WriteString('profile','performs','/connect '+wasteserv);
    fi.Free;
 end;
end;

procedure connect;
var
 wnd,wnd2,wnd3:Hwnd;
 buff: ARRAY [0..127] OF Char;
 rec:TRECT;
 top1:integer;
 ss:string;
begin
 wini;
 wnd:=findwindow('WASTEmainwnd',NIL);    // ������� ����
 if wnd=0 then
 begin // �� ��������
    winexec(pansichar('waste'), SW_SHOW);
    wnd:=findwindow('WASTEmainwnd',NIL);    // ������� ����
 end;
 GetWindowRect(Wnd, Rec);
 top1:=rec.Top;
 wnd3:=findwindowex(wnd,0,'Static',NIL); // ����� � ���. ���������
 while wnd3<>0 do
 begin
   GetWindowRect(Wnd3, Rec);
//   if ((rec.Top-top1=46)and(IswindowVisible(wnd)))or
//      ((rec.Top-top1=7)and(not IswindowVisible(wnd))) then
//   begin
      GetWindowText(wnd3,buff,length(buff));
      mystrcopy(ss,buff);
      if Copy(ss,1,1)=':' then
      begin
         if trim(ss)<>':0' then exit; // ��� ���� �������
         break;
      end;
   wnd3:=GetWindow(wnd3, gw_hWndNext);
 end;

 wnd:=findwindow('WASTEnetwnd',NIL);   // ���� �����������
 GetWindowRect(Wnd, Rec);
 top1:=rec.Top;
 if wnd=0 then exit; 
 showwindow(wnd,SW_SHOW);
 wnd2:=findwindowex(wnd,0,'ComboBox',NIL);  // ������ ��������
 sendmessage(wnd2,CB_SETCURSEL,0,0);        // ������� ���. ������� ������
 sendmessage(wnd,WM_ACTIVATE,1,0);          // ���������� ����
 wnd3:=findwindowex(wnd,0,'Button',NIL);    // ���� ������ "�����������"
 while wnd3<>0 do
 begin
   GetWindowRect(Wnd3, Rec);
   if (abs(rec.top-rec.Bottom)=26)and(abs(rec.Right-rec.left)=29)and(rec.Top-top1<=35) then
   begin
      sendmessage(wnd3,WM_LBUTTONDOWN,0,0);  // ������ � �������� ������
      sendmessage(wnd3,WM_LBUTTONUP,0,0);
      break;
   end;
   wnd3:=GetWindow(wnd3, gw_hWndNext);
 end;
 showwindow(wnd,SW_HIDE);
end;

procedure Search(SR: string);
var
 wnd,wnd2,wnd3:Hwnd;
 buff: ARRAY [0..127] OF Char;
 rec:TRECT;
 //top1:integer;
 ss:string;
 Menu1, menu2: THandle;
 Item2: UINT;
begin
 wnd:=findwindow('WASTEmainwnd',NIL);    // ������� ����
 showwindow(wnd,SW_SHOW);
 sendmessage(wnd,WM_ACTIVATE,1,0);
 Menu1 := GetMenu(wnd);           // ������� ����
 if Menu1 <> 0 then
 begin
    //Item1 := GetMenuItemID(Menu1, 1); // view
    Menu2 := GetSubMenu(Menu1,1);
    Item2 := GetMenuItemID(Menu2, 1);  // browser
    if Item2 <> 0 then
    begin
       SendMessage(wnd, WM_COMMAND, Item2, 0);
    end;
 end;
 ///
 showwindow(wnd,SW_HIDE);
 wnd:=findwindow('WASTEsearchwnd',NIL);    // ���� ������
 showwindow(wnd,SW_SHOW);
 sendmessage(wnd,WM_ACTIVATE,1,0);          // ���������� ����
 setwindowpos(wnd,HWND_TOP,100,50,screen.Width-200,screen.Height-100,SWP_SHOWWINDOW);
 getWindowRect(Wnd, Rec);
 //top1:=rec.Top;

 if sr='' then exit;

 ss:=sr+#0;
 strcopy(buff,pchar(sr));
 wnd2:=findwindowex(wnd,0,'ComboBox',NIL);  // ������ ������ combobox
 wnd2:=findwindowex(wnd2,0,'Edit',NIL);  // Edit ������������� combobox
 sendmessage(wnd2,WM_SETTEXT, 0, longint(@buff));

 wnd3:=findwindowex(wnd,0,'Button','Search:'); // ������ �����
 if wnd3<>0 then
 begin
    sendmessage(wnd3,WM_LBUTTONDOWN,0,0);  // ������ � �������� ������
    sendmessage(wnd3,WM_LBUTTONUP,0,0);
 end;

end;

procedure transfer;
var
 wnd:HWND;
begin
 wnd:=findwindow('WASTExferwnd',NIL);    // ���� ������
 showwindow(wnd,SW_SHOW);
 setwindowpos(wnd,HWND_TOP,100,150,screen.Width-200,screen.Height-300,SWP_SHOWWINDOW);
 sendmessage(wnd,WM_ACTIVATE,1,0);          // ���������� ����
end;

procedure DisConnect;
type
LV_ITEM= record
    mask:UINT;
    iItem:integer;
    iSubItem:integer;
    state:UINT;
    stateMask:UINT;
    pszText:LPTSTR;
    cchTextMax:integer;
    iImage:integer;       // index of the list view item's icon
    lParam:LPARAM;       // 32-bit value to associate with item
end;
var
 wnd,wnd2,wnd3:HWND;
 rec:TRECT;
 cnt:integer;
 i:integer;
 top1,left1:integer;
 buff: ARRAY [0..127] OF Char;
 ss:string;
 lvItem:LV_ITEM;
const
 LVIF_STATE = $0008;
 LVM_FIRST = $1000;
 LVM_GETITEMCOUNT = LVM_FIRST + 4;
 LVS_EX_FULLROWSELECT    = $00000020;
 LVM_SETITEMSTATE        = LVM_FIRST + 43;
 LVIS_FOCUSED            = $0001;
 LVIS_SELECTED           = $0002;
 LVIS_CUT                = $0004;
 LVIS_DROPHILITED        = $0008;
 LVIS_ACTIVATING         = $0020;

 sel = -1;
begin
 wnd:=findwindow('WASTEmainwnd',NIL);    // ������� ����
 GetWindowRect(Wnd, Rec);
 top1:=rec.Top;
 wnd3:=findwindowex(wnd,0,'Static',NIL); // ����� � ���. ���������
 while wnd3<>0 do
 begin
   GetWindowRect(Wnd3, Rec);
   if ((rec.Top-top1=46)and(IswindowVisible(wnd)))or
      ((rec.Top-top1=7)and(not IswindowVisible(wnd))) then
   begin
      GetWindowText(wnd3,buff,length(buff));
      mystrcopy(ss,buff);
      if trim(ss)=':0' then exit; // ��� ���������
      break;
   end;
   wnd3:=GetWindow(wnd3, gw_hWndNext);
 end;

 wnd:=findwindow('WASTEnetwnd',NIL);   // ���� �����������
 showwindow(wnd,SW_SHOW);
 GetWindowRect(Wnd, Rec);
 top1:=rec.Top;
 left1:=rec.Left;

 wnd2:=findwindowex(wnd,0,'SysListView32',NIL);  // ������ ��������

 wnd3:=findwindowex(wnd,0,'Button',NIL);    // ���� ������ "��������������"
 while wnd3<>0 do
 begin
   GetWindowRect(Wnd3, Rec);
   if (abs(rec.top-rec.Bottom)=26)and(abs(rec.Right-rec.left)=29)
      and(rec.Top-top1=56)and(rec.Left-left1=73) then
   begin
      break;
   end;
   wnd3:=GetWindow(wnd3, gw_hWndNext);
 end;

 sendmessage(wnd,WM_ACTIVATE,1,0);
 sendmessage(wnd2,WM_ACTIVATE,1,0);
 cnt:=sendmessage(wnd2,LVM_GETITEMCOUNT,0,0);
 lvItem.mask := LVIF_STATE;
 lvItem.state :=  LVIS_DROPHILITED; //LVIS_SELECTED; //LVIS_FOCUSED;
 lvItem.stateMask := $00FF;
 for i:=0 to cnt do
 begin
     sendmessage(wnd2,LVM_SETITEMSTATE,i,Longint(@lvItem));    // ���������� ������ ������
 end;
 sendmessage(wnd,WM_ACTIVATE,1,0);
 sendmessage(wnd2,WM_ACTIVATE,1,0);
 sendmessage(wnd2,WM_LBUTTONUP,1,0);
{ sendmessage(wnd2,WM_RBUTTONDOWN,1,0);
 enablewindow(wnd3,true);
}
 sendmessage(wnd2,WM_MOUSEACTIVATE,HTCLIENT,0);
// sendmessage(wnd2,WM_LBUTTONDOWN,0,0);
 sendmessage(wnd2,WM_LBUTTONUP,0,0);
 enablewindow(wnd3,true);
 sendmessage(wnd3,WM_LBUTTONDOWN,0,0);  // ������ � �������� ������
 sendmessage(wnd3,WM_LBUTTONUP,0,0);
 sendmessage(wnd3,WM_LBUTTONDOWN,0,0);  // ������ � �������� ������
 sendmessage(wnd3,WM_LBUTTONUP,0,0);
end;

procedure rescan;
var
 wndp,wnd,wnd2,wnd3:HWND;
 rec:TRECT;
 //cnt:integer;
 //top1,left1:integer;
 //buff: ARRAY [0..127] OF Char;
 Menu1, menu2: THandle;
 Item2: UINT;
begin
 wnd:=findwindow('WASTEmainwnd',NIL);    // ������� ����
 showwindow(wnd,SW_SHOW);
 sendmessage(wnd,WM_ACTIVATE,1,0);
 Menu1 := GetMenu(wnd);           // ������� ����
 if Menu1 <> 0 then
 begin
    //Item1 := GetMenuItemID(Menu1, 0); // File
    Menu2 := GetSubMenu(Menu1,0);
    //cnt:=GetMenuItemCount(menu2);
    Item2 := GetMenuItemID(Menu2, 0);  // preferences
    if Item2 <> 0 then
    begin
       SendMessage(wnd, WM_COMMAND, Item2, 0);
    end;
 end;
 wnd2:=findwindow('WASTEwnd',NIL);   // ���� preferences
 wndp:=wnd2;
 showwindow(wnd2,SW_SHOW);
 sendmessage(wnd2,WM_ACTIVATE,1,0);
 GetWindowRect(Wnd2, Rec);
 //top1:=rec.Top;
 //left1:=rec.Left;
 wnd2:=findwindowex(wnd2,0,'#32770',NIL);    // ���� 'Sending files'  ��� '#32770' ��������� �� ������ ����
 wnd3:=findwindowex(wnd2,0,'Button','Rescan');    // ���� ������ 'Rescan'
 GetWindowRect(Wnd3, Rec);
 if wnd3<>0 then
 begin
    sendmessage(wnd3,WM_LBUTTONDOWN,0,0);  // ������ � �������� ������
    sendmessage(wnd3,WM_LBUTTONUP,0,0);
 end;
 delay(1000);
 showwindow(wndp,SW_HIDE);
 showwindow(wnd,SW_HIDE);
end;

end.
