library waste;

uses
  Windows,
  //Messages,
  SysUtils,
  Classes,
  Forms,
  Dialogs,
  graphics,
  api in '..\..\api.pas',
  wastecontrol in 'wastecontrol.pas';

{$R plugin.RES}

var
  Plg: IPluginInterface;
  // ������� �� ������ �������
  main_panel:pchar;
  btn1, btn2, btn3, btn4:pchar;
  edit1:pchar;
  vr:string;

Function PluginName:pchar; forward;

// ���������� ��� �������� (������������)
Function PluginName:pchar;
begin
 PluginName:='waste';
end;

// ���������� ������ �������� (������������)
Function PluginVer:pchar;
begin
 PluginVer:='1.3';
end;

// ������������ �������� ��� �������� dll
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
    // Application:=DA;
  end;
end;

// �������� �� ������ �� ������
Procedure knopka1click(sender:tobject);
var
 edit_search:string;
begin
 connect;
 edit_search:=plg.GetText(PluginName,edit1);
 search(edit_search);
 plg.settext(PluginName,edit1,'');
end;

// ���������� ������� ������ � edit
procedure editkeydown(var Key: Word; Shift: TShiftState);
begin
  if key=VK_RETURN then knopka1click(nil);
end;

// �������� �� ������ �� ������
Procedure knopka2click(sender:tobject);
begin
 connect;
 transfer;
end;

// �������� �� ������ �� ������
Procedure knopka3click(sender:tobject);
begin
 connect;
end;

// �������� �� ������ �� ������
Procedure knopka4click(sender:tobject);
begin
 connect;
 rescan;
end;

// ����������� �������
Procedure ExtractRes(path:string);
var
  fl:string;
  i:integer;
  ResStream:TResourceStream;
  FileStream :TFileStream;
begin
  for i:=1 to 4 do
  begin
    fl:=path+'b'+inttostr(i)+'.bmp';
    ResStream := TResourceStream.Create(hinstance, pchar('B'+inttostr(i)), 'MY');
    try
       try
        FileStream := TFileStream.Create(fl, fmCreate);
        try
          FileStream.CopyFrom(ResStream, 0);
        finally
          FileStream.Free;
        end;
       except
       end;
    finally
        ResStream.Free;
    end;
  end;
end;

// ������������� ������� ���������� ��� �������� (������������)
Function Init(app:Tapplication):boolean;
var
  bm:Tbitmap;
  path:string;
  rz:boolean;
  st:TmemoryStream;
  hh,tp:integer;
begin
 rz:=false;
 try
   try
     // �������� ���������
     if not App.MainForm.GetInterface(IPluginInterface,Plg) then
     begin
        ShowMessage('�� ������� �������� ������ �� ���������!');
        result:=false;
        exit;
     end;
     // ����� �������
     path:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
     CreateDir(path);
     // ������ ��������
     vr:=plg.GetVar('ver');
     // ����������� �������
     ExtractRes(path);
{  bm:=Tbitmap.create;
  bm.LoadFromResourceName(Application.Handle,'B0');
  st:=TmemoryStream.Create;
  bm.SaveToStream(st);}
     hh:=22;   // ������ ������
     tp:=(plg.GetPluginHeight div 2)-(hh div 2);
     // ������ �������
     Plg.getPluginHeight;
     main_panel:=Plg.plugin_panel(PluginName);
     plg.SetWindow(PluginName,main_panel,-1,-1,220,-1);
     // ���� � ������
     edit1:=plg.Create_Control(PluginName,'tedit',main_panel);
     plg.setwindow(PluginName,edit1, 4, tp, 100, hh);
     plg.SetText(PluginName,edit1,'');
     plg.SetOnKeyDown(PluginName,edit1,addr(editkeydown));
     // ������ 1
     btn1:=plg.create_Control(PluginName,'tspeedbutton',main_panel);
     plg.setwindow(PluginName,btn1, 110, tp, hh, hh);          // ������� � ���������
     plg.SetGlyphFile(PluginName,btn1,pchar(path+'b1.bmp'));   // �������� �� ������ 1
   //  plg.SetGlyphStream(PluginName,btn1,st);
     plg.SetOnclick(PluginName, btn1, addr(knopka1click));     // ���������� ������ 1
     plg.SetFlat(PluginName, btn1, true);                     // ������� ������
     plg.SetHint(PluginName, btn1,'�����');
     // ������ 2
     btn2:=plg.create_Control(PluginName,'tspeedbutton',main_panel);
     plg.setwindow(PluginName,btn2, 135, tp, hh, hh);
     plg.SetGlyphFile(PluginName,btn2,pchar(path+'b2.bmp'));
     plg.SetOnclick(PluginName, btn2, addr(knopka2click));
     plg.SetFlat(PluginName, btn2, true);
     plg.SetHint(PluginName, btn2,'���� Transfer');
     // ������ 3
     btn3:=plg.create_Control(PluginName,'tspeedbutton',main_panel);
     plg.setwindow(PluginName,btn3, 160, tp, hh, hh);
     plg.SetGlyphFile(PluginName,btn3,pchar(path+'b3.bmp'));
     plg.SetOnclick(PluginName, btn3, addr(knopka3click));
     plg.SetFlat(PluginName, btn3, true);
     plg.SetHint(PluginName, btn3,'��������������');
     // ������ 4
     btn4:=plg.create_Control(PluginName,'tspeedbutton',main_panel);
     plg.setwindow(PluginName,btn4, 185, tp, hh, hh);
     plg.SetGlyphFile(PluginName,btn4,pchar(path+'b4.bmp'));
     plg.SetOnclick(PluginName, btn4, addr(knopka4click));
     plg.SetFlat(PluginName, btn4, true);
     plg.SetHint(PluginName, btn4,'���������������');
     // �����
     rz:=true;
   except
     rz:=false;
   end;
 finally
   result:=rz;
 end;
end;

{����� ����� �������� �������}
Procedure Setup;
begin
 showmessage('Setup click');
end;

exports
 Init,
 PluginName,
 PluginVer;
begin
  { ���-������ ���, ��� ������ ����������� �
    �������� ������������� ���������� }
  DLLProc := @MyDLLProc;
end.
