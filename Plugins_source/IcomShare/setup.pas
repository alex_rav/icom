unit setup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, inifiles,
  JvBaseDlg, JvBrowseFolder, Vcl.ComCtrls, Vcl.FileCtrl, Vcl.Samples.Spin;

type
  TShareSetupForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    AddBtn: TBitBtn;
    DelBtn: TBitBtn;
    ListBox1: TListBox;
    RescanBtn: TBitBtn;
    JvBrowseForFolderDialog1: TJvBrowseForFolderDialog;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    SpinEdit2: TSpinEdit;
    procedure AddBtnClick(Sender: TObject);
    function GetSelected:integer;
    procedure DelBtnClick(Sender: TObject);
    procedure rescan;
    procedure savescan(list:Tstringlist);
    procedure scan(dr:string; list:Tstringlist);
    procedure RescanBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ShareSetupForm: TShareSetupForm;

implementation

{$R *.dfm}

uses shareglobal;
{
procedure TSetupForm.scan(dr:string; list:Tstringlist);
var
 sr:TsearchRec;
 procedure Add(ss:string);
 begin
   if (ss>'')and(ss<>'.')and(ss<>'..') then
     list.Add(ss);
 end;
begin
  Add('<dir>'+dr+'</dir>');
  if FindFirst(dr+'\*.*', faAnyfile, sr) = 0 then
  begin
    if ((sr.Attr and faDirectory)<>0) and (sr.name<>'.') and (sr.name<>'..') then
       scan(dr+'\'+sr.name, list)
    else if ((sr.Attr and faDirectory) = 0) then
       Add('<file>'+sr.name+'</file><size>'+inttostr(sr.size)+'</size>');
    while FindNext(sr) = 0 do begin
      if ((sr.Attr and faDirectory)<>0) and (sr.name<>'.') and (sr.name<>'..') then
        scan(dr+'\'+sr.name, list)
      else if ((sr.Attr and faDirectory) = 0) then
        add('<file>'+sr.name+'</file><size>'+inttostr(sr.size)+'</size>');
    end;
  end;
  FindClose(sr);
end; }
procedure TShareSetupForm.scan(dr:string; list:Tstringlist);
var
 sr:TsearchRec;
 procedure Add(ss:string);
 begin
   if (ss>'')and(ss<>'.')and(ss<>'..') then
     list.Add(ss);
 end;
begin
  if copy(dr,length(dr),1)=SLASH then
    dr := copy(dr,1,length(dr)-1);
  add('<d>'+dr+'</d>');
  if FindFirst(dr+'\*.*', faAnyfile, sr) = 0 then
  begin
    if ((sr.Attr and faDirectory)<>0) and (sr.name<>'.') and (sr.name<>'..') then
       scan(dr+SLASH+sr.name, list)
    else if ((sr.Attr and faDirectory) = 0) then
       Add('<f>'+dr+SLASH+sr.name+'</f><s>'+inttostr(sr.size)+'</s>');
    while FindNext(sr) = 0 do
    begin
      if ((sr.Attr and faDirectory)<>0) and (sr.name<>'.') and (sr.name<>'..') then
        scan(dr+SLASH+sr.name, list)
      else if ((sr.Attr and faDirectory) = 0) then
        add('<f>'+dr+SLASH+sr.name+'</f><s>'+inttostr(sr.size)+'</s>');
    end;
  end;
  FindClose(sr);
end;

procedure TShareSetupForm.SpeedButton1Click(Sender: TObject);
var
  dir:string;
begin
  dir := Edit1.text;
  if SelectDirectory('','', dir, [], nil)  then
    Edit1.text := dir;
end;

procedure TShareSetupForm.savescan(list:Tstringlist);
begin
  list.SaveToFile(plgpath+fileslist);
end;

procedure TShareSetupForm.rescan;
var
  lst:Tstringlist;
  i:integer;
begin
  statusbar1.SimpleText := 'Scaning...';
  lst:=Tstringlist.Create;
  for i := 0 to listbox1.Count-1 do
    scan(listbox1.Items[i],lst);
  savescan(lst);
  statusbar1.SimpleText := 'Scaning end';
end;

procedure TShareSetupForm.RescanBtnClick(Sender: TObject);
begin
  rescan;
end;

procedure TShareSetupForm.DelBtnClick(Sender: TObject);
begin
  if GetSelected>=0 then
    listbox1.DeleteSelected;
end;

procedure TShareSetupForm.FormActivate(Sender: TObject);
begin
  statusbar1.SimpleText := '';
end;

procedure TShareSetupForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  downloadSpeed := StrToInt(SpinEdit1.Text);
  uploadSpeed := StrToInt(SpinEdit2.Text);
end;

function TShareSetupForm.GetSelected:integer;
var
  i:integer;
begin
  result:=-1;
  for i:=0 to listbox1.Count-1 do
    if listbox1.Selected[i] then
    begin
      result := i;
      break;
    end;
end;

procedure TShareSetupForm.AddBtnClick(Sender: TObject);
var
  dir:string;
begin
  if GetSelected>=0 then
    Dir:=listbox1.Items[GetSelected]
  else
    Dir:='';
  if SelectDirectory('','', dir, [], nil)  then
  begin
    try
      listbox1.Items.Add(dir);
    except // no duplicat
    end;
  end;
end;

end.
