unit downloader;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Buttons, IdTCPConnection, IdHTTP,
  IdBaseComponent, IdComponent,
  Vcl.ComCtrls,
  IdInterceptThrottler, StrUtils, SyncObjs;
const
  speed1m=1*1024*1024*8; // 1 �����/���

type
  Turl = class(TObject)
    public
      url: string;
      localfile: string;
  end;

  TDownloader=class
    private
      IdHTTP:TIdHTTP;
      Throttler:TIdInterceptThrottler;
      max: Int64;
    public
      canceled: boolean;
      startTime: TDateTime;
      lastTime: TDateTime;
      ResponseText: string;
      ResponseCode: integer;
      ready: boolean;
      pc: integer;   // file %
      ip: string;
      filename: string;
      url: string;
      urls: TstringList;
      maxspeed: integer;
      procedure IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
      procedure IdHTTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
      procedure IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
      procedure FileDownload(url, localFile:string);
      procedure doFileDownload(url, localFile:string);
      //procedure DirDownload(baseDir, fromDir:string);
      procedure Cancel;
      constructor Create;
      destructor Destroy; override;
  end;

implementation

uses shareglobal, ithreads;

constructor TDownloader.Create;
begin
  urls := TstringList.Create;
  startTime := Now;
  lastTime := Now;
end;

destructor TDownloader.Destroy;
begin
  urls.Free;
end;

procedure TDownloader.Cancel;
begin
  canceled := true;
  if Assigned(IdHttp)and Assigned(IdHTTP.Socket) then
    IdHTTP.Socket.Close;
end;

procedure TDownloader.FileDownload(url, localFile:string);
var
  th: Tithread;
begin
  th:=Tithread.Create;
  th.FreeOnTerminate := True;
  th.url := url;
  th.localfile := localfile;
  th.dProc := dofiledownload;
  th.Start;
end;

// �������� ����� � ����
procedure TDownloader.DoFileDownload(url, localFile:string);
var
  stream:TFileStream;
begin
  ip := getIP(url);
  filename := localFile;
  Stream:=TFileStream.Create(localFile, fmCreate or fmShareDenyWrite );
  Throttler:=TIdInterceptThrottler.Create(nil);
  IdHTTP:=TIdHTTP.Create(nil);
  idHTTP.OnWorkBegin := IdHTTPWorkBegin;
  idHTTP.OnWorkEnd := IdHTTPWorkEnd;
  idHTTP.OnWork := IdHTTPWork;
  try
    Throttler.BitsPerSec:=maxspeed;
    IdHTTP.CreateIOHandler();
    IdHTTP.IOHandler.Intercept:=Throttler;
    try
      IdHTTP.Get(url,Stream);
      if idHTTP.ResponseCode=200 then
      begin
        if not canceled then
          pc := 100
        else
          pc := -1;
        ready := true;
      end;
    except
      on e: exception do
      begin
        ResponseText := idHTTP.ResponseText;
        ResponseCode := idHTTP.ResponseCode;
        pc := 999;
        ready := true;
      end;
    end;
  finally
    Throttler.Free;
    IdHTTP.Free;
    Stream.Free;
  end;
  sleep(0);
end;

(*
// �������� �������� (�������� �� ������)
procedure TDownloader.DirDownload(baseDir, fromDir:string);
var
  stream:TFileStream;
  i: integer;
  urlFile: string;
  localPath, localFile: string;
begin
  pc := 0;
  for i := 0 to urls.Count-1 do
  begin
    if canceled then
      break;
    urlFile := urls[i];
    ip := getIP(urlFile);
    localPath := Extractfilepath(urldecode(copy(urlFile,pos('file=',urlFile)+5)));
    localPath := baseDir+InternalDir(fromDir, DelDiskName(localPath));
    localPath := stringreplace(localPath,'\\','\',[rfreplaceall]);
    CreateFullPath(localPath);
    localFile := extractfilename(urldecode(urlFile));
    localFile := localPath+localFile;
    //self.urlFile ��� url ��������
    {Stream:=TFileStream.Create(localFile, fmCreate or fmShareDenyWrite);
    Throttler:=TIdInterceptThrottler.Create(nil);
    IdHTTP:=TIdHTTP.Create(nil);
    idHTTP.OnWorkBegin := IdHTTPWorkBegin;
    idHTTP.OnWorkEnd := IdHTTPWorkEnd;
    idHTTP.OnWork := IdHTTPWork;
    try
      Throttler.BitsPerSec:=maxspeed;
      IdHTTP.CreateIOHandler();
      IdHTTP.IOHandler.Intercept:=Throttler;
      try
        IdHTTP.Get(urlFile, Stream);
        if idHTTP.ResponseCode=200 then
        begin
        end;
      except
        on e: exception do
        begin
        end;
      end;
    finally
      Throttler.Free;
      IdHTTP.Free;
      Stream.Free;
    end;  }
    FileDownload(urlFile, localFile);
    while (not FileExists(localFile))or(MyFileSize(localFile)=0) do
    begin
      sleep(200);
      vcl.forms.Application.ProcessMessages;
    end;
    ready := False;
    pc := trunc(100 * ((i+1)/urls.Count));
  end;
  if not canceled then
    pc := 100
  else
    pc := -1;
  ready := true;
end;
*)
procedure TDownloader.IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
begin
  if urls.Count=0 then
    pc := trunc(100*AWorkCount/max);
  lastTime := Now;
end;

procedure TDownloader.IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  if urls.Count=0 then
    pc := 0;
  Max := AWorkcountMax;
  lastTime := Now;
end;

procedure TDownloader.IdHTTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
end;

end.
