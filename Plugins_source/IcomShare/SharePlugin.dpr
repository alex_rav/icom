library SharePlugin;

uses
  SysUtils,
  Classes,
  windows,
  forms,
  inifiles,
  controls,
  share in 'share.pas' {shareform},
  api in '..\..\api.pas',
  setup in 'setup.pas' {ShareSetupForm},
  userslist in '..\..\userslist.pas',
  shareglobal in 'shareglobal.pas',
  fileslist in 'fileslist.pas',
  Global in '..\..\Global.pas' {},
  downloader in 'downloader.pas',
  icomView in '..\..\icomView.pas',
  IEbrowser in '..\..\IEbrowser.pas',
  MSHTML_TLB in '..\..\Lib\MSHTML_TLB.pas',
  ithreads in 'ithreads.pas',
  wbpopup in 'wbpopup.pas',
  webserver in 'webserver.pas' {HttpForm},
  ieredimer in '..\..\Lib\ieredimer.pas';

var
  main_btn:WideString;
  mainapp:TApplication;

{ $R shareplugin.res}
{$R .\RESOURCE\plugin.RES}

Function PluginName:widestring; forward;

// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
  PluginName:='Share plugin';
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
  PluginVer:='0.96';
end;

// ������������ �������� ��� �������� dll  (������������)
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
  end;
end;


{����� ����� �������� �������}
Procedure Setup;
begin
  ShareSetupForm.ShowModal;
end;

// �������� �� ������ �� ������
Procedure pluginclick(sender:tobject);
begin
  if ShareForm.WindowState=wsMinimized then
    ShareForm.WindowState:=wsNormal;

  mainapp.MainForm.SendToBack;
  ShareForm.Show;
  ShareForm.HomeBtnClick(nil);
  sleep(1500);
  ShareForm.BringToFront;
end;

procedure extract(fname, resname: string);
var
  ResStream: TResourceStream;
  FileStream: TFileStream;
begin
  ResStream := TResourceStream.Create(hinstance, resname, 'MY');
  try
    FileStream := TFileStream.Create(fname, fmCreate);
    try
      FileStream.CopyFrom(ResStream, 0);
    finally
      FreeAndNil(FileStream);
    end;
  except
  end;
  FreeAndNil(ResStream);
end;

// ����������� �������
Procedure ExtractRes(path:string);
begin
  extract(path+'b1.bmp','B1');
  extract(path+'folder.gif','B2');
  extract(path+'folderup.gif','B3');
  extract(path+'reload.gif','B4');
  extract(path+'user.gif','B5');
  extract(path+'cancel.gif','B6');
  extract(path+'play.gif','B7');
end;

procedure Done;
begin
  plg := nil;
  FreeAndNil(mydirs);
  FreeAndNil(myfiles);
  FreeAndNil(ShareSetupForm);
  FreeAndNil(ShareForm);
  FreeAndNil(HttpForm);
  FreeAndNil(uploads);
  FreeAndNil(downloads);
  FreeAndNil(mydirs);
  FreeAndNil(myfiles);
end;

// ������������� ������� ���������� ��� �������� (������������)
function Init(app:Tapplication):boolean;
var
  rz: boolean;
begin
  rz:=true;
  try
    one_sek := strtotime('0:0:1');
    myDirs := TStringList.Create;
    myFiles := TStringList.Create;
    // �������� ���������
    if not App.MainForm.GetInterface(IPluginInterface,Plg) then
    begin
      MessageBox(0, '�� ������� �������� ������ �� ���������!', 'Error', mb_ok);
      rz:=false;
    end
    else
    begin
      mainapp:=app;
      // ����� �������
      pname:=PluginName;
      plgpath:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
      apppath:=extractfilepath(application.exename);
      CreateDir(plgpath);
      // ����������� �������
      ExtractRes(plgpath);
      // ������ �������
      main_btn:=plg.plugin_button(PluginName);
      plg.SetGlyphFile(PluginName,main_btn, plgpath+'b1.bmp');
      plg.SetOnclick(PluginName, main_btn, addr(PluginClick));
      // ������
      myip:=plg.GetIp;
      port:=plg.GetPort+1;
      //
      ShareForm:=TShareForm.Create(nil);
      ShareSetupForm:=TShareSetupForm.Create(nil);
      HttpForm:=THttpForm.Create(nil);
      // ������
      uploads:=TFilesList.Create;
      downloads:=TFilesList.Create;
      // ���������
      loadsetup;
      //
      if rescanOnStart then
      begin
        ShareSetupForm.rescan;
        LoadFiles;
      end;
      rz:=true;
    end;
  finally
    result:=rz;
  end;
end;

Procedure DataDecode(packet: widestring);
begin
  share.getcommand(packet);
end;

function UrlProc(url: widestring):boolean;
begin
  result := shareform.urlproc(url);
end;

exports
 Init,
 Done,
 PluginName,
 PluginVer,
 DataDecode,
 Setup,
 UrlProc;

begin
  DLLProc := @MyDLLProc;
end.
