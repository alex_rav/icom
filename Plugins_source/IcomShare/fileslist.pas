unit fileslist;

interface

uses sysutils, classes, variants, downloader;

type
  TFileInfo = class
    public
      fname: string;
      url: string;
      Size: int64;
      Percent: integer;
      ResponseText: string;
      ResponseCode: integer;
      startTime: Tdatetime;
      lastTime: Tdatetime;
  end;

  TFilesList = class
  private
    fModified: boolean;
    files: TStringList;
    function GetFile(index:integer):TFileInfo;
    function GetModified:boolean;
  public
    property Modified:boolean read GetModified;
    property FileInfo[index:integer]:TFileInfo read GetFile;
    constructor Create;
    destructor Destruct;
    procedure Add(url, fname:string; size:int64);
    function Find(url:string):integer;
    function FindNotEnd(url:string):integer;
    procedure Update(url:string; pc:integer);
    procedure Update2(dl:Tdownloader);
    procedure Delete(url:string);
    function Count:integer;
    procedure NotModified;
    procedure Clear;
  end;

implementation

procedure TFilesList.Clear;
begin
  files.Clear;
  fModified := true;
end;

function TFilesList.GetFile(index:integer):TFileInfo;
begin
  result := TFileInfo(files.Objects[index]);
end;

function TFilesList.Count:integer;
begin
  result := files.Count;
end;

constructor TFilesList.Create;
begin
  inherited;
  files := TStringList.Create;
end;

destructor TFilesList.Destruct;
var
  i: integer;
begin
  inherited;
  for i:=0 to files.Count-1 do
    files.Objects[i].Free;
  FreeAndNil(files);
end;

procedure TFilesList.Add(url, fname:string; size:int64);
var
  fi:TFileInfo;
begin
  if (url>'')and(fname>'') then
  begin
    fi:=TFileInfo.Create;
    fi.fName := fname;
    fi.Size := size;
    fi.url := url;
    fi.Percent := 0;
    files.AddObject(fname, fi);
    fi.startTime := Now;
    fModified := true;
  end;
end;

function TFilesList.Find(url:string):integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to files.count-1 do
  begin
    if TFileInfo(files.objects[i]).url=url then
    begin
      result := i;
      break;
    end;
  end;
end;

function TFilesList.FindNotEnd(url:string):integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to files.count-1 do
  begin
    if (TFileInfo(files.objects[i]).url=url)
    and(TFileInfo(files.objects[i]).percent<>100)
    and(TFileInfo(files.objects[i]).percent>=0)
    and(TFileInfo(files.objects[i]).percent<>999) then
    begin
      result := i;
      break;
    end;
  end;
end;

procedure TFilesList.Update(url:string; pc:integer);
var
  index: integer;
begin
  index := FindNotEnd(url);
  if index>=0 then
  begin
    TFileInfo(files.objects[index]).Percent := pc;
    TFileInfo(files.objects[index]).lastTime := Now;
    fModified := true;
  end;
end;

procedure TFilesList.Update2(dl:Tdownloader);
var
  index: integer;
begin
  index := FindNotEnd(dl.url);
  if index>=0 then
  begin
    TFileInfo(files.objects[index]).Percent := dl.pc;
    TFileInfo(files.objects[index]).ResponseText := dl.ResponseText;
    TFileInfo(files.objects[index]).ResponseCode := dl.ResponseCode;
    TFileInfo(files.objects[index]).lastTime := Now;
    fModified := true;
  end;
end;

procedure TFilesList.Delete(url:string);
var
  index: integer;
begin
  index := Find(url);
  if index>=0 then
  begin
    files.delete(index);
    fModified := true;
  end;
end;

function TFilesList.GetModified:boolean;
begin
  result := fModified;
end;

procedure TFilesList.NotModified;
begin
  fModified := false;
end;

end.
