﻿unit WbPopup;

interface

var
  href: string;

implementation

uses Windows,Controls,Messages,ShDocVw,activex,
  MSHTML_TLB, strutils;

var
  HMouseHook:THandle;

// удаление запрещенных символов
function delsym(msg: string): string;
const
  todel = '><|?*/\:"=;#()[]';
var
  i: integer;
  ss: string;
begin
  ss := '';
  for i := 1 to length(msg) do
  begin
    if pos(msg[i], todel) = 0 then
      ss := ss + msg[i];
  end;
  result := ss;
end;

// Функция перевода шестнадцетиричного символа в число
function HexToInt(CH: char): integer;
begin
  result := 0;
  case CH of
    '0' .. '9':
      result := Ord(CH) - Ord('0');
    'A' .. 'F':
      result := Ord(CH) - Ord('A') + 10;
    'a' .. 'f':
      result := Ord(CH) - Ord('a') + 10;
  end;
end;

// Функция перевода шестнадцетиричного символа в число
function ToHex(Value: integer): string;
var
  stb, mlb: integer;
const
  hex = '0123456789ABCDEF';
begin
  stb := Value div 16;
  mlb := Value - 16 * stb;
  result := hex[stb + 1] + hex[mlb + 1];
end;


function urlEncode(Value: string; Force: boolean = false): string;
var
  i: integer;
  CH: ansichar;
  ss: string;
  aValue: ansistring;
begin
  aValue := ansistring(Value);
  ss := '';
  if length(aValue) <> 0 then
    for i := 1 to length(aValue) do
    begin
      CH := aValue[i];
      if Ord(CH) >= 128 then // русские
      begin
        ss := ss + '%' + ToHex(Ord(byte(CH)));
      end
      else if Ord(CH) = $20 then
        ss := ss + '+'
      else if Force then // все символы перекодировать
        ss := ss + '%' + ToHex(Ord(CH))
      else
        ss := ss + char(CH);
    end;
  result := ss;
end;

// Преобразует символы, записанные в виде URLencoded
function urlDecode(Value: string): string;
var
  i, L: integer;
  utf: string;
begin
  if Value > '' then
  begin
    utf := '';
    L := 0;
    for i := 1 to length(Value) do
    begin
      if (Value[i] <> '%') and (Value[i] <> '+') and (L < 1) then
      begin
        utf := utf + string(Value[i]);
      end
      else
      begin
        if (Value[i] = '+') then
          utf := utf + ' '
        else if (Value[i] = '%') then
        begin
          L := 2;
          if (i < length(Value) - 1) then
          begin
            utf := utf + string(ansichar(HexToInt(Value[i + 1]) * 16 + HexToInt(Value[i + 2])));
          end;
        end
        else
          Dec(L);
      end;
    end;
    result := utf;
  end
  else
    result := '';
end;

function GetElementAtPos(Doc: IHTMLDocument2; x, y: integer): IHTMLElement;
begin
  Result := nil;
  Result := Doc.elementFromPoint(x, y);
end;

function GetNick(WC: TWebBrowser; P: TPoint): string;
var
  Doc: IHTMLDocument2;
  Element: IHTMLElement;
  x, y: Integer;
begin
  result := '';
  x := P.X;
  y := P.Y;
  Doc := WC.Document as IHTMLDocument2;
  Element := GetElementAtPos(doc, x, y);
  if Assigned(Element) then
  begin
    if Element.innerHTML = Delsym(Element.innerHTML) then
      result := Element.innerHTML;
  end;
end;

function GetElement(WC: TWebBrowser; P: TPoint): string;
var
  Doc: IHTMLDocument2;
  Element: IHTMLElement;
  x, y: Integer;
  ps1,ps2,ps3: Integer;
  ss: string;
begin
  result := '';
  x := P.X;
  y := P.Y;
  // Get the element under the mouse cursor
  Doc := WC.Document as IHTMLDocument2;
  Element := GetElementAtPos(doc, x, y);
  if Assigned(Element) then
  begin
    ss := Element.outerhtml;
    ps1 := Pos('href=', ss);
    if (ps1=0)and(Element.parentelement<>nil) then
      ss := Element.parentelement.outerhtml;
    ps1 := Pos('href=', ss);
    if ps1<>0 then
    begin
      ps2 := posex('"',ss,ps1);
      ps3 := posex('"',ss,ps2+1);
      ss := Copy(ss,ps2+1,ps3-ps2-1);
      Result := urldecode(ss);
    end;
  end;
end;

function MouseProc(
    nCode: Integer;     // hook code
    WP: wParam; // message identifier
    LP: lParam  // mouse coordinates
   ):Integer;stdcall;
var MHS:TMOUSEHOOKSTRUCT;
    WC:TWinControl;
    P:TPoint;
begin
  Result:=CallNextHookEx(HMouseHook,nCode,WP,LP);
  if nCode=HC_ACTION then
   begin
     MHS:=PMOUSEHOOKSTRUCT(LP)^;
     if ((WP=WM_RBUTTONDOWN) or (WP=WM_RBUTTONUP)) then
      begin
        WC:=FindVCLWindow(MHS.pt);
        if (WC is TWebBrowser) then
        begin
          // на чем кликнули?
          P:=WC.ScreenToClient(MHS.pt);
          href := urlEncode(GetElement(TWebBrowser(WC), P));
          //
          Result:=1;
          if (TWebBrowser(WC).PopupMenu<>nil) and (WP=WM_RBUTTONUP) then
           begin
            TWebBrowser(WC).PopupMenu.PopupComponent:=WC;
            TWebBrowser(WC).PopupMenu.Popup(MHS.pt.x,MHS.pt.y);
           end;
        end;
      end;
   end;
end;

initialization


  HMouseHook:=SetWindowsHookEx(WH_MOUSE,@MouseProc,HInstance,GetCurrentThreadID);
  OleInitialize(nil);


finalization

  OleUninitialize;
  if HMouseHook <> 0 then  UnHookWindowsHookEx(HMouseHook);

end.