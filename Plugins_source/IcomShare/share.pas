unit share;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls,
  IdComponent,
  api, Vcl.ComCtrls, Vcl.Menus, icomView, IdBaseComponent, IdCustomTCPServer;

type
  TShareForm = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    findbtn: TBitBtn;
    Panel3: TPanel;
    setupBtn: TBitBtn;
    HomeBtn: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    LoadPopupMenu: TPopupMenu;
    CopyLinkMenu: TMenuItem;
    LoadLinkMenu: TMenuItem;
    TabSheet3: TTabSheet;
    TrafPopupMenu: TPopupMenu;
    ClearTrafMenu: TMenuItem;
    procedure setupBtnClick(Sender: TObject);
    procedure HomeBtnClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ShowTraf;
    procedure CopyLinkMenuClick(Sender: TObject);
    procedure LoadLinkMenuClick(Sender: TObject);
    procedure HTML1BeforeNavigate(ASender: TObject;
          const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
          Headers: OleVariant; var Cancel: WordBool);
    procedure HTML2BeforeNavigate(ASender: TObject;
          const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
          Headers: OleVariant; var Cancel: WordBool);
    procedure findbtnClick(Sender: TObject);
    function urlProc(pUrl:widestring):boolean;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure doGet(ip, fl, url:string);
    procedure FormDestroy(Sender: TObject);
    procedure ClearTrafMenuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    dlist: TStringList;
    popupUrl: string;
  public
    { Public declarations }
    Html1: TIcomViewer;
    Html2: TIcomViewer;
    Html3: TIcomViewer;
  end;

var
  ShareForm: TShareForm;
  lastdata: Tdatetime;
  finddata: string;

Procedure GetCommand(packet: widestring);

implementation

{$R *.dfm}

uses setup, global, strutils, shareglobal, shellapi,
  downloader, clipbrd, wbpopup;

procedure Log(msg:string);
var
 lf:string;
 flog:textfile;
begin
  shareform.StatusBar1.SimpleText := msg;
  try
    lf:=plgpath+pname+'.log';
    assignfile(flog,lf);
    if fileexists(lf) then
      append(flog)
    else
      rewrite(flog);
    writeln(flog,datetostr(date)+' '+timetostr(time)+' '+msg);
    closefile(flog);
  except
  end;
end;

function getsl(List: TStringList; name: string): string;
var
  I: integer;
begin
  result := '';
  if (name > '') then // ������� �� �����
  begin
    name := '#' + name;
    for I := 0 to List.count - 1 do
      if List.strings[I] = name then
      begin
        result := List.strings[I + 1];
        Break;
      end;
  end;
end;

procedure SendCommand(ip, cmd, fl: string; data:string='');
var
 pkg: string;
begin
  pkg:= '#SHARE' + LF + LF +
        '#COMMAND' + LF + cmd + LF +
        '#FILENAME' + LF + urlencode(fl) + LF +
        '#DATA' + LF + base64encode(data) + LF;
  plg.TCPSend(ip, pkg);
end;

procedure ListDownload(ip:string);
var
  fname: string;
  dl: TDownloader;
  url: string;
  i: integer;
begin
  url :=  'http://'+ip+':'+inttostr(port)+'/file='+pname+'_data\ALLLIST';
  fname := nick(ip)+'.lst';

  dl := TDownloader.Create;
  dl.maxspeed := downloadSpeed * speed1m;
  dl.url := url;
  // � ������
  Downloads.Add(url, fname,0);
  // ������ �����������
  shareform.dlist.AddObject(url, dl);
  // ��������� ��������
  dl.FileDownload(url, DownloadDir+fname);
  // ���� ����
  for i := 0 to 10 do
  begin
    if shareform.dlist.IndexOf(url)<0 then
      break;
    sleep(1000);
    application.ProcessMessages;
  end;
end;

// �������� �����
procedure FileDownload(url:string);
var
  fname: string;
  ip: string;
  ps1, ps2: integer;
  dl: TDownloader;
begin
  ps1 := pos('//',url);
  ps2 := posex(':',url,ps1+2);
  ip := copy(url, ps1+2, ps2-ps1-2);
  fname := copy(url, pos('file=',url)+5);
  fname := urldecode(fname);

  dl := TDownloader.Create;
  dl.maxspeed := downloadSpeed * speed1m;
  dl.url := url;

  // � ������
  Downloads.Add(url, fname,0);
  // ������ �����������
  shareform.dlist.AddObject('file', dl);
  // ��������� ��������
  dl.FileDownload(url, DownloadDir + Extractfilename(fname));
  // �����������
  Downloads.Update(url, dl.pc);
end;

// �������� ��������
procedure DirDownload(url:string);
var
  ip: string;
  ps2: integer;
  dlDir: TDownloader;
  dlFile: TDownloader;
  i: integer;
  startLevel: integer;
  dir: string;
  afiles: TStringlist;
  dfiles: TStringlist;
  urlFile:string;
  localPath, localFile: string;
  fromdir: string;
  pc: Integer;
begin
  ps2 := pos(':',url);
  ip := copy(url, 1, ps2-1);
  dir := copy(url, pos('LIST:',url)+5);
  dir := urldecode(dir);
  // ��������� ������ ������ � ��������
  ListDownload(ip);
  if not fileexists(DownloadDir+nick(ip)+'.lst') then
  begin
    log('Error: no files list');
    exit;
  end
  else
  begin
    afiles := TStringlist.Create;
    afiles.LoadFromFile(DownloadDir+nick(ip)+'.lst');
  end;

  dlDir := TDownloader.Create;
  dlDir.maxspeed := downloadSpeed * speed1m;
  dlDir.url := url;

  dfiles := TStringlist.Create;
  // ��� ����� � �������� � ����
  startLevel := levelCount(dir);
  for i := 0 to afiles.Count-1 do
  begin
      if (pos(dir, afiles.strings[i]) <> 0)and(pos('<f>',afiles.strings[i])<>0) then
        if levelCount(GetTag('f',afiles.strings[i]))-1 >= startLevel then // � ������
          dfiles.Add('http://'+ip+':'+inttostr(port)+'/file='+urlencode(GetTag('f',afiles.strings[i]),true));

  end;
  afiles.Free;
  // � ������
  Downloads.Add(url,dir,0);
  // ������ �����������
  shareform.dlist.AddObject(url, dlDir);
  // ��������� ��������
  for i := 0 to dfiles.Count-1 do
  begin
    if dlDir.Canceled then
      Break;
    urlFile := dfiles.Strings[i];
    ip := getIP(urlFile);
    localPath := Extractfilepath(urldecode(copy(urlFile,pos('file=',urlFile)+5)));
    fromDir := lastdir(dir);
    localPath := DownloadDir+InternalDir(fromDir, DelDiskName(localPath));
    localPath := stringreplace(localPath,'\\','\',[rfreplaceall]);
    CreateFullPath(localPath);
    localFile := extractfilename(urldecode(urlFile));
    localFile := localPath+localFile;

    dlFile := TDownloader.Create;
    dlFile.maxspeed := downloadSpeed * speed1m;
    dlFile.url := urlFile;
    dlFile.FileDownload(urlFile, localFile);
    while (not dlFile.ready) do // ��������� ����� �� �������
    begin
      sleep(200);
      vcl.forms.Application.ProcessMessages;
    end;
    pc := trunc(100 * ((i+1)/dfiles.Count));
    dlDir.pc := pc;
    dlDir.lastTime := Now;
    dlFile.Free;
    // �����������
    //Downloads.Update(url, pc);
  end;
  dlDir.ready := true;
  dfiles.free;
end;

function html5video(fileName:string):string;
begin
  Result :=
  '<video width="400" height="250" controls="controls" autoplay>'+
  '<source src="http://'+myip+':'+inttostr(port)+'/PLAY='+urlencode(filename)+'" type="'+getContentType(filename)+'"</source>'+
  ' Your browser does not support the video'+
  '</video>';
end;

function html5audio(fileName:string):string;
begin
  Result :=
  '<audio controls autoplay>'+
  '<source src="http://'+myip+':'+inttostr(port)+'/PLAY='+urlencode(filename)+'" type="'+getContentType(filename)+'"</source>'+
  ' Your browser does not support the audio'+
  '</audio>';
end;

function html5(fileName:string):string;
begin
  if pos('video/', getContentType(filename))<>0 then
    result := html5video(filename)
  else
    result := html5audio(filename);
end;

Procedure GetCommand(packet: widestring);
var
  sl: TStringList;
  signature: string;
  fromip: string;
  cmd: string;
  fname: string;
  data: string;
begin
  sl := TStringList.Create;
  try
    sl.Text := packet;
    signature := sl.strings[0];
    if signature = '#SHARE' then
    begin
      FromIP := getsl(sl, 'REALIP');
      cmd := uppercase(getsl(sl, 'COMMAND'));
      fname := urldecode(getsl(sl, 'FILENAME'));
      log(nick(fromip)+' '+cmd+' '+DelDiskName(fname));
      // ������ - ����� ������
      if CMD = 'FIND' then
      begin
        finddata := '';
        data := FileList(myIp, '*'+fname, myFiles);
        if data > '' then
          SendCommand(FromIP, cmd+'DATA', fname, data);
      end
      // ������ ��������� ������
      else if CMD = 'FINDDATA' then
      begin
        data := base64decode(getsl(sl, 'DATA'));
        finddata := finddata + data;
        ShareForm.html1.LoadFromString(htmlbegin+img2local(finddata)+htmlend);
        ShareForm.Html1.Refresh;
      end
      // ������ - ������ ������ � ��������
      else if CMD = 'LIST' then
      begin
        data := FileList(myIp, fname, myFiles);
        SendCommand(FromIP, cmd+'DATA', fname, htmlbegin+data+htmlend);
      end
      // ������ ������ ��������
      else if CMD = 'LISTDATA' then
      begin
        data := base64decode(getsl(sl, 'DATA'));
        ShareForm.html1.LoadFromString(img2local(data));
        ShareForm.Html1.Refresh;
        savetofile(plgpath+'listdata.html', img2local(data));
      end
      else if cmd = 'UPLOAD' then
      begin
        Uploads.Add(fromIP, fname, myfilesize(fname));
      end
      else if cmd = 'PLAY' then
      begin
        data := html5(fname);
        SendCommand(FromIP, cmd+'DATA', fname, htmlbegin(ExtractFileName(fname))+data+htmlend);
      end
      else if cmd = 'PLAYDATA' then
      begin
        data := base64decode(getsl(sl, 'DATA'));
        savetofile(plgpath+'play.html', data);
        shellExecute(0, 'OPEN', PChar(plgpath+'play.html'), '', '', SW_SHOW);
      end;
    end;
  finally
    sl.free;
  end;
  Application.ProcessMessages;
end;

procedure PutStringIntoClipBoard(const Str: WideString);
var
  Size: Integer;
  Data: THandle;
  DataPtr: Pointer;
begin
  Size := Length(Str);
  if Size = 0 then
    exit;
  if not IsClipboardFormatAvailable(CF_UNICODETEXT) then
    Clipboard.AsText := Str
  else
  begin
    Size := Size shl 1 + 2;
    Data := GlobalAlloc(GMEM_MOVEABLE + GMEM_DDESHARE, Size);
    try
      DataPtr := GlobalLock(Data);
      try
        Move(Pointer(Str)^, DataPtr^, Size);
        Clipboard.SetAsHandle(CF_UNICODETEXT, Data);
      finally
        GlobalUnlock(Data);
      end;
    except
      GlobalFree(Data);
      raise;
    end;
  end;
end;

// ip:command>file
procedure TShareForm.CopyLinkMenuClick(Sender: TObject);
var
 ip: string;
 url: string;
 cmd: string;
 uname: string;
 ps1, ps2: integer;
begin
  popupUrl := wbpopup.href;
  ps1 := pos(':',popupUrl);
  ps2 := posex(':',popupUrl,ps1+1);
  ip := copy(popupUrl,1,ps1-1);
  uname := users.GetNickName(ip);
  cmd := copy(popupUrl, ps1+1, ps2-ps1-1);
  url := urldecode(copy(popupUrl, ps2+1));
  PutStringIntoClipboard(ansilowercase('share://'+urlencode(uname+':'+cmd+':'+Url, true)));
end;

procedure TShareForm.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = VK_RETURN)and(trim(edit1.Text)>'') then
    findbtnClick(Sender);
end;

procedure TShareForm.findbtnClick(Sender: TObject);
var
  i: integer;
begin
  for i := 0 to Users.Count-1 do
    if (users.Get(i).Status<>OFFLINE)and(pos(PName, users.Get(i).plugins)<>0) then
      SendCommand(users.Get(i).ip,'FIND', trim(edit1.text));
end;

procedure TShareForm.HTML2BeforeNavigate(ASender: TObject;
      const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
var
  url2: string;
  bHandled: boolean;
  fname: string;
  i: integer;
begin
    bHandled := false;
  if pos('cancel://', URL)<>0 then
  begin
    url2 := copy(URL, length('cancel://')+1);
    url2 := urldecode(url2);
    url2 := DelLast(url2);
    for i := 0 to dlist.Count-1 do
      if TDownloader(dlist.Objects[i]).url = url2 then
      begin
        log('cancel: '+DelDiskName(url2));
        TDownloader(dlist.Objects[i]).Cancel;
      end;
    bHandled := true;
  end
  else
  begin
    url2 := urldecode(URL);
    url2 := DelLast(url2);
    fname := copy(url2, pos('//', url2)+2);
    if url2 > '' then
    begin
        if (extractfilepath(url2)=htmldir)and(uppercase(extractfileext(url2))='.HTML') then
          bHandled := false
        else // ����� � ��������
        begin
          Exec(fname);
          bHandled := true;
        end;
    end;
  end;
  cancel := bHandled;
end;

procedure TShareForm.FormActivate(Sender: TObject);
begin
  Edit1.SetFocus;
end;

procedure TShareForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveSetup;
end;

procedure TShareForm.FormCreate(Sender: TObject);
begin
  pagecontrol1.ActivePageIndex := 0;
  onesek := strtotime('0:0:1');
  lastdata := date;
  dlist := TStringList.Create;
  Html1 := TIcomViewer.Create(TabSheet1);
  Html1.OnBeforeNavigate2 := HTML1BeforeNavigate;
  Html1.PopupMenu := LoadPopupMenu;
  Html2 := TIcomViewer.Create(TabSheet2);
  Html2.OnBeforeNavigate2 := HTML2BeforeNavigate;
  Html2.PopupMenu := TrafPopupMenu;
  Html3 := TIcomViewer.Create(TabSheet3);
  Html3.OnBeforeNavigate2 := HTML2BeforeNavigate;
  Html3.PopupMenu := TrafPopupMenu;
end;

procedure TShareForm.FormDestroy(Sender: TObject);
begin
  dlist.free;
end;

procedure TShareForm.HomeBtnClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  ShareForm.html1.LoadFromString(img2local(MainHtml));
  log('');
end;

procedure TShareForm.doGet(ip, fl, url:string);
var
  i: integer;
begin
  if messagedlg('���������? '+DelDiskName(urldecode(fl)),mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    for i := 0 to dlist.Count-1 do
      if TDownloader(dlist.Objects[i]).url = url then
      begin
        ShowMessage('���� ���� ��� �����������');
        exit;
      end;
    log('start: '+DelDiskName(urldecode(fl)));
    try
      FileDownload(url);
    except
      on e: exception do
        log('error: '+e.Message);
    end;
  end;
end;

procedure TShareForm.HTML1BeforeNavigate(ASender: TObject;
      const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
var
  src: string;
  ip, cmd, fl: string;
  ps1, ps2: integer;
  url2: string;
begin
  src := url;
  ps1:=pos(':',src);
  ps2:=posex(':', src, ps1+1);
  ip:=copy(src, 1, ps1-1);
  cmd:=copy(src, ps1+1, ps2-ps1-1);
  fl:=copy(src, ps2+1, length(src)-ps2);
  url2 := 'http://'+ip+':'+inttostr(port)+'/file='+fl;
  if cmd='GET' then
  begin
    doGet(ip, fl, url2);
  end
  else
    sendcommand(ip, cmd, fl);
  cancel:=true;
end;

procedure TShareForm.LoadLinkMenuClick(Sender: TObject);
var
  ip, cmd, fl: string;
  ps1, ps2: integer;
  i: integer;
  url: string;
  src: string;
begin
  popupurl := wbpopup.href;
  src:=popupUrl;
  ps1:=pos(':',src);
  ps2:=posex(':', src, ps1+1);
  ip:=copy(src, 1, ps1-1);
  cmd:=copy(src, ps1+1, ps2-ps1-1);
  fl:=copy(src, ps2+1, length(src)-ps2);
  if cmd='GET' then  // ���� ����
  begin
    url := 'http://'+ip+':'+inttostr(port)+'/file='+fl;
    doGet(ip, fl, url);
  end
  else  // ���� �������
  begin
    if messagedlg('���������? '+DelDiskName(urldecode(fl)),mtConfirmation,[mbYes,mbNo],0)=mrYes then
    begin
      for i := 0 to dlist.Count-1 do
        if TDownloader(dlist.Objects[i]).url = url then
        begin
          ShowMessage('���� ������� ��� �����������');
          exit;
        end;
      log('startDir: '+DelDiskName(urldecode(fl)));
      try
        DirDownload(src);
      except
        on e: exception do
          log('error: '+e.Message);
      end;
    end;
  end;
end;

procedure TShareForm.ClearTrafMenuClick(Sender: TObject);
begin
  if pagecontrol1.activepageindex = 1 then
    downloads.Clear
  else
    uploads.Clear;
end;

procedure TShareForm.setupBtnClick(Sender: TObject);
begin
  if ShareSetupForm=nil then
    ShareSetupForm:=TShareSetupForm.Create(self);
  ShareSetupForm.Position:=poScreenCenter;
  ShareSetupForm.ShowModal;
  SaveSetup;
  LoadSetup;
end;

procedure TShareForm.ShowTraf;
var
  ss: string;
begin
  ss := Downloads2html;
  html2.LoadFromString(img2local(ss));
  savetofile(plgpath+'download.html', ss);
  ss := Uploads2html;
  html3.LoadFromString(img2local(ss));
  savetofile(plgpath+'upload.html', ss);
end;

procedure TShareForm.Timer1Timer(Sender: TObject);
var
  i:integer;
begin
  i := 0;
  while i <= dlist.Count-1 do
  begin
    Downloads.Update2(TDownloader(dlist.Objects[i]));
    if TDownloader(dlist.Objects[i]).ready then
    begin
      log('ready: '+Nick(getip(TDownloader(dlist.Objects[i]).ip))+' '+DelDiskName(extractfilename(TDownloader(dlist.Objects[i]).filename)));
      dlist.Objects[i].Free;
      dlist.Objects[i] := nil;
      dlist.Delete(i);
    end
    else if now-TDownloader(dlist.Objects[i]).lastTime>60*one_sek then  // error timeout
    begin
      TDownloader(dlist.Objects[i]).pc := 999;
      dlist.Objects[i].Free;
      dlist.Objects[i] := nil;
      dlist.Delete(i);
      i := i+1;
    end
    else if now-TDownloader(dlist.Objects[i]).lastTime>10*60*one_sek then  //garbage collection
    begin
      dlist.Objects[i].Free;
      dlist.Objects[i] := nil;
      dlist.Delete(i);
      i := i+1;
    end
    else
      i := i+1;
  end;
  if (self.Visible)and((Downloads.Modified)or(uploads.Modified)) then
    ShowTraf;
end;

// share://����:�������:c:\common\!test_data.txt
function TShareForm.urlProc(pUrl:widestring):boolean;
var
  ps1, ps2, ps3: integer;
  uname: string;
  ip: string;
  cmd: string;
  url, url2: string;
  fl: string;
begin
  if pos('SHARE://', ansiuppercase(purl)) <> 0 then
  begin
    url := dellast(urldecode(purl));
    ps1 := pos(':', url);
    ps2 := posex(':', url, ps1+1);
    ps3 := posex(':', url, ps2+1);
    uname := copy(url, ps1+3, ps2-ps1-3);
    ip := users.GetIP(uname);
    cmd := uppercase(copy(url, ps2+1, ps3-ps2-1));
    fl := copy(url, ps3+1);
    if cmd = 'GET' then
    begin
      self.Show;
      url2 := 'http://'+ip+':'+inttostr(port)+'/file='+urlencode(fl,true);
      doGet(ip, fl, url2);
      pagecontrol1.ActivePageIndex := 1;
    end
    else if cmd = 'PLAY' then
    begin
      url2 := 'http://'+ip+':'+inttostr(port)+'/play='+urlencode(fl,true)+'&rand='+inttostr(GetTickCount);
      ShellExecute(0, 'OPEN', PChar(url2), '', '', SW_SHOW);
    end
    else if cmd = 'LIST' then
    begin
      self.Show;
      sendcommand(ip, cmd, urlencode(fl, true));
      pagecontrol1.ActivePageIndex := 0;
    end;
    result := true;
  end
  else
    result := false;
end;

end.
