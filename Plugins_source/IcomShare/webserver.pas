unit webserver;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdBaseComponent, IdComponent,
  IdCustomTCPServer, IdCustomHTTPServer, IdHTTPServer, IdContext, IdIntercept,
  IdInterceptThrottler, IdGlobal;

type
  THttpForm = class(TForm)
    HTTPServer: TIdHTTPServer;
    procedure HTTPServerCommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure FormCreate(Sender: TObject);
    procedure HTTPServerConnect(AContext: TIdContext);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    IdInterceptThrottler1: TIdInterceptThrottler;
    procedure LoadSetup;
    function inDirs(dir:string):boolean;
  public
    { Public declarations }
    procedure StartHttp;
  end;

var
  HttpForm: THttpForm;
  dirs: TStringlist;
  dirs_time: Tdatetime;

implementation

uses inifiles, shareglobal, share;

{$R *.dfm}

procedure THttpForm.LoadSetup;
var
  fl:Tinifile;
  i:integer;
  dd:string;
  d_t: Tdatetime;
  //mx: Integer;
begin
  FileAge(plgpath+'Share plugin.ini', d_t);
  if dirs_time = d_t then
    Exit;
  Dirs.Clear;
  fl:=Tinifile.Create(plgpath+'Share plugin.ini');
  for i := 0 to 100 do
  begin
    dd:=fl.ReadString('dirs','dir'+inttostr(i),'');
    if dd>'' then
      Dirs.Add(ansiuppercase(dd));
  end;
  //mx := fl.ReadInteger('local', 'maxspeed', 3);
  fl.free;
  FileAge(plgpath+'Share plugin.ini', dirs_time);
end;

procedure THttpForm.StartHttp;
const
  speed=5*1024*1024*8; // 5 �����/���
begin
  HttpServer.Active := false;
  HttpServer.DefaultPort := Port;
  HttpServer.Active := true;
end;

procedure THttpForm.FormCreate(Sender: TObject);
begin
  dirs := TStringList.Create;
  LoadSetup;
  StartHttp;
end;

function Thttpform.inDirs(dir:string):boolean;
var
  i:integer;
begin
  LoadSetup;
  result := false;
  for i:= 0 to dirs.Count-1 do
  begin
    if pos(dirs.Strings[i], dir) = 1 then
    begin
      result := true;
      break;
    end;
  end;
end;

procedure THttpForm.FormDestroy(Sender: TObject);
begin
  HTTPServer.Active := false;
  dirs.Free;
end;

function extractPath(url:string):string;
var
  i:integer;
begin
  for i := length(url) downto 1 do
    if url[i] = '/' then
    begin
      result := copy(url,1,i);
      break;
    end;
end;

function fileinserver(url:string):string;
var
  i:Integer;
begin
  for i := 0 to dirs.Count-1 do
  begin
    if fileexists(dirs.Strings[i] + url) then
    begin
      result := dirs.Strings[i] + url;
      break;
    end;
  end;
end;


procedure THttpForm.HTTPServerCommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  Filename: string;
  Dir: string;
  url: string;
  fromIP: string;
  msg: string;
  spec: boolean;
  play:Boolean;
  ps1, ps2:Integer;
  i:Integer;
  key:string;
  FS: TFileStream;
  Buf: TIdBytes;
  BufLen: Integer;
const
  keys:array[0..1] of string = ('FILE', 'PLAY');
  playkey = 1;
begin
  // ������ �� ����� �����
  fromIP := ARequestInfo.remoteip;
  if users.GetID(fromIP)<0 then
    exit;
  // bandwidth limit
  IdInterceptThrottler1 := TIdInterceptThrottler.Create(self);
  IdInterceptThrottler1.BitsPerSec := uploadSpeed * speed1m;
  AContext.Connection.IOHandler.Intercept := IdInterceptThrottler1;
  // http://ip:port/command=file_name[&var=value]
  url := urldecode(ARequestInfo.uri);
  // ������� ������ ������
  if (fileinserver(url)>'') then
  begin
    AResponseInfo.ResponseNo := 200;
    try
      filename := fileinserver(url);
      if isImage(fileName) then
        AresponseInfo.ContentType := 'image/jpeg'
      else
        AresponseInfo.ContentType := 'application/octet-stream';
      AResponseInfo.ContentStream := TFileStream.Create(filename, fmOpenRead + fmShareDenyWrite);
    except
      on e: exception do
      begin
        AResponseInfo.ResponseNo := 500;
        AResponseInfo.ContentText := e.Message;
      end;
    end;
    Exit;
  end
  else  // �������
  begin
    ps2 := Pos('&', url);
    for i := 0 to Length(keys) do
    begin
      play := (Pos(uppercase('/'+keys[playkey]+'='), uppercase(url))<>0);
      key := keys[i];
      ps1 := pos(uppercase('/'+key+'='), UpperCase(url));
      if ps1 <> 0 then
      begin
        if ps2 = 0 then
          url := copy(url, ps1+length(key)+2)
        else
          url := copy(url, ps1+length(key)+2, ps2-ps1-length(key)-2);
        if url > '' then
          Break;
      end;
    end;
  end;
  url := stringreplace(url,'/','\',[rfReplaceall]);
  dir := AnsiUppercase(extractfilepath(url));
  filename := extractfilename(url);
  if play then
  begin
    { // v1
      AResponseInfo.ResponseNo := 200;
      AResponseInfo.ContentType := getContentType(url);
      if AResponseInfo.ContentType = '' then
        AResponseInfo.ContentType := AResponseInfo.HTTPServer.MIMETable.GetFileMIMEType(FileName);
      AResponseInfo.ContentStream := TFileStream.Create(url, fmOpenRead or fmShareDenyWrite);
    }
    // v2
    FS := TFileStream.Create(url, fmOpenRead or fmShareDenyWrite);
    try
      AResponseInfo.ResponseNo := 200;
      AResponseInfo.ContentType := getContentType(url);
      if AResponseInfo.ContentType = '' then
        AResponseInfo.ContentType := AResponseInfo.HTTPServer.MIMETable.GetFileMIMEType(FileName);
      AResponseInfo.TransferEncoding := 'chunked';
      AResponseInfo.WriteHeader;
      SetLength(Buf, 1024);
      repeat
        BufLen := FS.Read(Buf[0], 1024);
        if BufLen < 1 then Break;
        AContext.Connection.IOHandler.WriteLn(IntToHex(BufLen, 1));
        AContext.Connection.IOHandler.Write(Buf, BufLen);
        AContext.Connection.IOHandler.WriteLn;
      until False;
      AContext.Connection.IOHandler.WriteLn('0');
      AContext.Connection.IOHandler.WriteLn;
    finally
      FS.Free;
    end;
  end
  else
  begin
    if filename='ALLLIST' then
    begin
      filename := apppath+'plugins\'+dir+'files.lst';
      url := filename;
      spec := true;
    end
    else
      spec := false;
    // ������ ����������� ��������
    if (not inDirs(dir))and(not spec) then
    begin
      AResponseInfo.ResponseNo := 403;
      AResponseInfo.ContentText := 'Forbidden: ' + ARequestInfo.Document;
    end
    else if FileExists(url) then
    begin
      msg:='#SHARE' + LF + LF +
           '#COMMAND' + LF + 'UPLOAD'+ LF +
           '#FILENAME' + LF + urlencode(url) + LF +
           '#REALIP' + LF + fromIP + LF;
      GetCommand(msg);

      AResponseInfo.ResponseNo := 200;
      try
        AresponseInfo.ContentType := 'application/octet-stream';
        AResponseInfo.ContentStream := TFileStream.Create(url, fmOpenRead + fmShareDenyWrite);
      except
       on e: exception do
       begin
         AResponseInfo.ResponseNo := 500;
         AResponseInfo.ContentText := e.Message;
       end;
      end;
    end
    else
    begin
      AResponseInfo.ResponseNo := 404;
      AResponseInfo.ContentText := 'The requested URL ' + ARequestInfo.Document
       + ' was not found on this server.';
    end;
  end;
end;


procedure THttpForm.HTTPServerConnect(AContext: TIdContext);
begin
   ;
end;

end.
