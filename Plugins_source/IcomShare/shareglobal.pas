unit shareglobal;

 {$IFNDEF VCL_6_OR_ABOVE}
    {$DEFINE WINDOWS}
  {$ENDIF}

interface

uses windows, forms, shellapi, userslist, classes, sysutils, inifiles, api, IdCoderMIME, fileslist, math, strutils;

procedure SaveSetup;
procedure LoadSetup;
procedure LoadFiles;
procedure LoadUsers;

function HtmlBegin(title:string=''):string;
function HtmlEnd:string;
function Users2html:string;
function MainHtml:string;
function FileList(ip, dir:string; files:TStringList):string;
function Downloads2html:string;
function Uploads2html:string;
procedure savetofile(fname, data:string);

function MyFileSize(FileName: string): Longint;
function GetTag(tag, value:string): string;
function Base64Encode(const Text: String): String;
function Base64Decode(const Text: String): String;
function urlEncode(Value: string; Force: boolean = false): string;
function urlDecode(Value: string): string;
function toint(ss: string): int64;
function DelDiskName(dir: string):string;
function DelFirst(dir: string):string;
function DelLast(dir: string):string;
function nick(ip:string):string;
function levelcount(dir: string):integer;
procedure CreateFullPath(path:string);
function LastDir(path:string):string;
function InternalDir(baseDir, dir:string):string;
procedure Exec(url:string);
function img2local(page:string):string;
function GetIP(url:string):string;
function getContentType(fileName:string):string;
function isImage(filename: string):boolean;


var
  Plg: IPluginInterface;
  PName: string;
  plgpath:string;
  apppath:string;
  myip:string;
  port:integer;
  users:TUsers;
  mydirs:TstringList;
  myfiles:TStringList;
  uploads:TFilesList;
  downloads:TFilesList;
  DownloadDir:string;
  onesek:double;
  rescanOnStart: boolean;
  one_sek: Extended;
  downloadSpeed: Integer;
  uploadSpeed: Integer;

const
  {$IFDEF WINDOWS}
  SLASH='\';
  {$ELSE}
  SLASH='/';
  {$ENDIF}
  filesList='files.lst';
  LF=#13#10;

const
  maxLevel=100;
  maxdir=100;
  block_size=1*1024*1024;
  default_share='c:\common\';
  default_download='c:\download\';
  speed1m=1024*1024*8; // 1 �����/���

implementation

uses setup, Global;

function img(fname:string):string; forward;

function isImage(filename: string):boolean;
var
  ext: string;
begin
  ext := uppercase(extractfileext(filename));
  if (ext='.GIF') or (ext = '.PNG') or (ext = '.JPG') or (ext = '.JPEG') or (ext = '.BMP') or (ext = '.ICO')then
    result := true
  else
    result := false;
end;

function File2string(file_name:string):ansistring;
var
  MyStream :TStringStream;
  TmpStrA: ansistring;
begin
  if FileExists(file_name) then
  begin
    MyStream := TStringStream.create;
    try
      MyStream.LoadFromFile(file_name);
      MyStream.Position := 0;
      if MyStream.Size > 0 then
      begin
        TmpStrA := AnsiString(MyStream.DataString);
      end
      else
        TmpStrA := '';
      result:=TmpStrA;
    finally
      MyStream.free;
    end;
  end
  else
    TmpStrA := '';
  result:=TmpStrA;
end;

function isOGGAudio(file_name: string):boolean;
var
  TmpStrA: ansistring;
  mybuf: array [0 .. 50] of Byte;
  i: integer;
begin
  TmpStrA := File2string(file_name);
  i := 0;
  while (i < Length(mybuf)) and (i < Length(TmpStrA)) do
  begin
    mybuf[i] := Byte(TmpStrA[i + 1]);
    i := i + 1;
  end;
  result := (mybuf[0] = $4F) and (mybuf[1] = $67) and (mybuf[2] = $67) and (mybuf[3] = $53);
end;

function getContentType(fileName:string):string;
const
  extv:array[0..2,0..1]of string=(('MP4','video/mp4'),('WebM','video/webm'),('Ogg','video/ogg'));
  exta:array[0..2,0..1]of string=(('MP3','audio/mpeg'),('Ogg','audio/ogg'),('Wav','audio/wav'));
var
  i:integer;
begin
  for i := 0 to Length(extv) do
  if (UpperCase(ExtractFileExt(fileName)) = UpperCase('.'+extv[i][0]))and(not isOGGAudio(fileName)) then
  begin
    Result := extv[i][1];
    Break;
  end;
  if result = '' then
  begin
    for i := 0 to Length(exta) do
    if UpperCase(ExtractFileExt(fileName)) = UpperCase('.'+exta[i][0]) then
    begin
      Result := exta[i][1];
      Break;
    end;
  end;
end;

function size2str(fsize:int64; showZero:boolean=true):string;
const
  kbt=1024;
  mbt=kbt*kbt;
  gbt=kbt*mbt;
var
  kb, mb, gb: real;
begin
  if fsize<>0 then
  begin
    gb := roundto(fsize/gbt, -2);
    fsize := fsize-trunc(gb)*gbt;
    mb := roundto(fsize/mbt, -1);
    fsize := fsize-trunc(mb)*mbt;
    kb := roundto(fsize/kbt, -1);
    if gb>1 then
      result := floattostr(gb)+' Gb'
    else if mb>1 then
      result := floattostr(mb)+' Mb'
    else if kb>1 then
      result := floattostr(kb)+' Kb'
    else
      result := inttostr(fsize)+ 'bytes';
  end
  else
  begin
    if showZero then
      result := '0 bytes'
    else
      result := '';
  end;
end;

function nick(ip:string):string;
begin
  result := users.GetNickName(ip);
end;

function toint(ss: string): int64;
begin
  if ss = '' then
    result := 0
  else
  begin
    result := StrToInt64(ss);
  end;
end;

function Base64Encode(const Text: String): String;
var
  Encoder: TIdEncoderMime;
begin
  Encoder := TIdEncoderMime.Create(nil);
  try
    result := Encoder.EncodeString(Text, TEncoding.ANSI);
  finally
    Freeandnil(Encoder);
  end
end;

function Base64Decode(const Text: String): String;
var
  Decoder: TIdDecoderMime;
begin
  Decoder := TIdDecoderMime.Create(nil);
  try
    try
      result := Decoder.DecodeString(Text, TEncoding.ANSI);
    except
      result := '';
    end;
  finally
    Freeandnil(Decoder)
  end
end;

procedure savetofile(fname, data:string);
var
  fl:TStringList;
begin
  fl:=TStringList.Create;
  fl.Text:=data;
  fl.SaveToFile(fname);
  fl.Free;
end;

// ������� �������� ������������������ ������� � �����
function HexToInt(CH: char): integer;
begin
  result := 0;
  case CH of
    '0' .. '9':
      result := Ord(CH) - Ord('0');
    'A' .. 'F':
      result := Ord(CH) - Ord('A') + 10;
    'a' .. 'f':
      result := Ord(CH) - Ord('a') + 10;
  end;
end;

// ������� �������� ������������������ ������� � �����
function ToHex(Value: integer): string;
var
  stb, mlb: integer;
const
  hex = '0123456789ABCDEF';
begin
  stb := Value div 16;
  mlb := Value - 16 * stb;
  result := hex[stb + 1] + hex[mlb + 1];
end;

// ����������� ������ � URLencoded
function urlEncode(Value: string; Force: boolean = false): string;
var
  i: integer;
  CH: ansichar;
  ss: string;
  aValue: ansistring;
begin
  aValue := ansistring(Value);
  ss := '';
  if length(aValue) <> 0 then
    for i := 1 to length(aValue) do
    begin
      CH := aValue[i];
      if Ord(CH) >= 128 then // �������
      begin
        ss := ss + '%' + ToHex(Ord(byte(CH)));
      end
      else if Ord(CH) = $20 then
        ss := ss + '+'
      else if Force then // ��� ������� ��������������
        ss := ss + '%' + ToHex(Ord(CH))
      else
        ss := ss + char(CH);
    end;
  result := ss;
end;

// ����������� �������, ���������� � ���� URLencoded
function urlDecode(Value: string): string;
var
  i, L: integer;
  utf: string;
begin
  if Value > '' then
  begin
    utf := '';
    L := 0;
    for i := 1 to length(Value) do
    begin
      if (Value[i] <> '%') and (Value[i] <> '+') and (L < 1) then
      begin
        utf := utf + string(Value[i]);
      end
      else
      begin
        if (Value[i] = '+') then
          utf := utf + ' '
        else if (Value[i] = '%') then
        begin
          L := 2;
          if (i < length(Value) - 1) then
          begin
            utf := utf + string(ansichar(HexToInt(Value[i + 1]) * 16 + HexToInt(Value[i + 2])));
          end;
        end
        else
          Dec(L);
      end;
    end;
    result := utf;
  end
  else
    result := '';
end;

function MyFileSize(FileName: string): Longint;
var
  SearchRec: TSearchRec;
begin
  if FindFirst(ExpandFileName(FileName), faAnyFile, SearchRec) = 0 then
    result := SearchRec.Size
  else
    result := -1;
  sysutils.FindClose(SearchRec);
end;

procedure LoadFiles;
begin
  MyFiles.Clear;
  if (not fileexists(plgpath+fileslist))or(myFileSize(plgpath+fileslist)=0) then
    ShareSetupForm.rescan;
  MyFiles.LoadFromFile(plgpath+fileslist);
  myFiles.Sort;
end;

procedure LoadUsers;
var
  us: TstringList;
  i: integer;
  uu, ip: string;
  ps: integer;
  ss: WideString;
begin
  if not assigned(users) then
    users := TUsers.Create
  else
    users.ClearUsers;
  us := TstringList.Create;
  ss := plg.GetUserList;
  us.Text := ss;
  for i := 0 to us.Count-1 do
  begin
    ps := pos('/',us.strings[i]);
    uu := copy(us.strings[i],1, ps-1);
    ip := copy(us.strings[i], ps+1, length(us.strings[i])-ps);
    users.AddUser(uu, ip);
    users.Get(users.Count - 1).name := uu;
    users.Get(users.Count - 1).ip := ip;
    users.Get(users.Count - 1).plugins := plg.GetUserPlugins(ip);
  end;
  // ������� ������
  for i := 0 to us.Count-1 do
    users.get(i).Status := plg.GetUserStatus(users.Get(i).ip);
end;

// �������� ���������
procedure LoadSetup;
var
  fl:Tinifile;
  i:integer;
  dd:string;
begin
  myDirs.Clear;
  fl:=Tinifile.Create(plgpath+PName+'.ini');
  try
    ShareSetupForm.listbox1.Clear;
    for i := 0 to maxdir do
    begin
      dd:=fl.ReadString('dirs','dir'+inttostr(i),'');
      if dd>'' then
      begin
        ShareSetupForm.listbox1.Items.Add(dd);
        myDirs.Add(dd);
      end;
    end;
    if ShareSetupForm.ListBox1.Count=0 then
    begin
      createdir(default_share);
      ShareSetupForm.listbox1.Items.Add(default_share);
      myDirs.Add(default_share);
    end;
    if myDirs.Count = 0 then
      createdir(default_share);
    downloadSpeed := fl.ReadInteger('local', 'downloadspeed', 5);
    uploadSpeed := fl.ReadInteger('local', 'uploadspeed', 3);
    DownloadDir := fl.ReadString('dirs', 'download', default_download);
    DownloadDir := DelLast(DownloadDir)+SLASH;
    ShareSetupForm.edit1.text := DownloadDir;
    rescanOnStart := fl.ReadBool('local', 'rescan', true);
    ShareSetupform.Edit1.Text := DownloadDir;
    ShareSetupForm.SpinEdit1.Text := IntToStr(downloadSpeed);
    ShareSetupForm.SpinEdit2.Text := IntToStr(uploadSpeed);
    createdir(DownloadDir);
    LoadUsers;
    LoadFiles;
  finally
    fl.Free;
  end;
end;

// �������� ���������
procedure SaveSetup;
var
  fl:Tinifile;
  i:integer;
begin
  fl:=Tinifile.Create(plgpath+PName+'.ini');
  try
    DownloadDir := trim(ShareSetupForm.Edit1.Text);
    fl.WriteString('dirs', 'download', DownloadDir);
    fl.WriteBool('local', 'rescan', reScanOnStart);
    for i := 0 to ShareSetupForm.listbox1.Count-1 do
      fl.WriteString('dirs','dir'+inttostr(i),ShareSetupForm.listbox1.Items[i]);
    for i := ShareSetupForm.listbox1.Count to maxdir do
      fl.WriteString('dirs','dir'+inttostr(i),'');
    fl.WriteString('dirs', 'download', DownloadDir);
    fl.WriteInteger('local', 'downloadspeed', downloadSpeed);
    fl.WriteInteger('local', 'uploadspeed', uploadSpeed);
  finally
    fl.Free;
  end;
end;

function getCodePage:string;
begin
  Result := 'Windows-'+inttostr(GetACP);
end;

function HtmlBegin(title:string=''):string;
begin
  result:=
  '<!doctype html>'+
  '<html>'+
  '<head>'+
  '<meta http-equiv="Content-Type" content="text/html; charset='+getCodePage+'">'+
  '<style> '+
  'A:link { color: blue; text-decoration: none; } '+
  'A:hover { color: red; text-decoration: none; } '+
  'A:visited { text-decoration: none; } '+
  '</style>';
  if title > '' then
    result := result + '<title>'+title+'</title>';
  result := result +
  '</head>'+
  '<body>';
end;

function HtmlEnd:string;
begin
  result:=
  '</body>'+
  '</html>';
end;

// ��������� �������� �� ������� ����������
function MainHtml:string;
begin
  LoadUsers;
  result:=
  HtmlBegin+
  Users2html+
  HtmlEnd;
end;

function Users2html:string;
var
  ht: string;
  i: integer;
begin
  ht:='<div>';
  for i := 0 to Users.Count-1 do
  begin
    if (users.Get(i).Status<>OFFLINE)and(pos(PName, users.Get(i).plugins)<>0) then
      ht := ht + '<a href="'+Users.Get(i).ip+':LIST:\'+'">' +img('user.gif')+ Users.Get(i).Name + '</a><br>';
  end;
  ht:=ht+'</div>';
  result:=ht;
end;

function GetTag(tag, value:string): string;
var
  ps1, ps2: integer;
  t1, t2:string;
begin
  t1 := '<'+tag+'>';
  t2 := '</'+tag+'>';
  ps1 := pos(t1, value);
  ps2 := pos(t2, value);
  result := copy(value, ps1+length(t1), ps2-ps1-length(t1));
end;

function DelDiskName(dir: string):string;
begin
  if pos(':',dir) <> 0 then
    result := copy(dir, pos(':',dir)+1)
  else
    result := dir;
end;

function DelFirst(dir: string):string;
begin
  result := copy(dir,2);
end;

function DelLast(dir: string):string;
begin
  if (copy(dir,length(dir))='\')or(copy(dir,length(dir))='/') then
    result := copy(dir,1,length(dir)-1)
  else
    result := dir;
end;

// �������� ���� �� 1 �������
function levelup(dir: string):string;
var
  ps1, i: integer;
begin
  dir := delLast(dir);
  ps1 := length(dir);
  for i := length(dir) downto 1 do
    if dir[i]='\' then
    begin
      ps1 := i;
      break;
    end;
  result := copy(dir, 1, ps1-1);
end;

function levelcount(dir: string):integer;
var
  i: integer;
  count: integer;
begin
  dir := delLast(dir);
  dir := stringreplace(dir, SLASH+SLASH, SLASH, [rfreplaceall]);
  count := 0;
  for i := 1 to length(dir) do
    if copy(dir,i,1) = SLASH then
      count := count + 1;
  result := count;
end;

function img2local(page:string):string;
begin
  result := stringreplace(page, 'src="', 'src="'+plgpath, [rfReplaceAll]);
end;

function img(fname:string):string;
begin
  result := '<img src="'+fname+'" border=0>';
end;

procedure SortList(var list:TStringList);
begin
  list.Sort;
end;

function DirSize(dir:string; files:TStringList):integer;
var
  i: integer;
  f: string;
begin
  Result := 0;
  for i := 0 to files.Count-1 do
  begin
    f := GetTag('f',files.strings[i]);
    if (pos(dir, files.strings[i]) <> 0)and(f > '') then
      result := Result + StrToInt(GetTag('s',files.Strings[i]));
  end;
end;

function isVideoAudio(fname:string):boolean;
var
  ext: string;
begin
  ext := UpperCase(ExtractFileExt(fname));
  result := (ext='.MP4')or(ext='.WEBM')or(ext='.OGG')or(ext='.MP3')or(ext='.WAV');
end;

function FileList(ip, dir:string; files:TStringList):string;
var
  i: integer;
  ht: string;
  currentLevel: integer;
  d, f: string;
  listcount: Integer;
  playLink:string;
begin
  listcount := 0;
  SortList(mydirs);
  SortList(files);
  if dir='\' then  // ������ ��� �����
  begin
    ht := ht + '<div style="background: silver; text-align:center; color:red;"><a href="'+ip+':LIST:\">'+img('user.gif')+Nick(ip)+'</a></div>';
    for i := 0 to myDirs.Count-1 do
    begin
      ht:=ht+'<a href="'+ip+':LIST:'+urlencode(mydirs.strings[i])+'">'+img('folder.gif') + copy(mydirs.strings[i],3) + '</a><br>'+LF;
      //listcount := listcount + 1;
    end;
  end
  else if pos('*', dir)<>0 then // �����
  begin
    dir := ansiuppercase(stringreplace(dir,'*','',[rfreplaceall]));
    // �������
    ht := ht + '<div style="background: silver; text-align:center; color:red;"><a href="'+ip+':FIND:'+urlencode(dir)+'">'+img('reload.gif')+nick(ip)+': '+dir+'</a></div>';
    // ��������
    for i := 0 to files.Count-1 do
    begin
      d := GetTag('d',files.strings[i]);
      if d >'' then
        if pos(dir,AnsiUpperCase(d)) <> 0 then
        begin
          ht:=ht+'<a href="'+ip+':LIST:'+urlencode(d)+'">'+img('folder.gif') + copy(d,3) + '</a> ' + '<br>'+LF;
          listcount := listcount +1;
        end;
    end;
    ht := ht + '<hr>';
    // �����
    for i := 0 to files.Count-1 do
    begin
      f :=  GetTag('f',files.strings[i]);
      if f > '' then
        if pos(dir, AnsiUpperCase(f)) <> 0 then
        begin
          if isVideoAudio(files.strings[i]) then
            playLink := ' <a href="'+ip+':PLAY:'+urlencode(f,true)+'">' + img('play.gif') + '</a> '
          else
            playLink := '';
          ht:=ht+'<a href="'+ip+':GET:'+urlencode(f,true)+'">' + DelDiskName(f) + '</a> ' + size2str(toint(GetTag('s',files.strings[i]))) + playLink +'<br>'+LF;
          listcount := listcount +1;
        end;
    end;
    if listcount = 0 then
      ht := '';
  end
  else
  begin  // �������
    currentLevel := levelCount(dir);
    // �������
    ht := ht + '<div style="background: silver; text-align:center; color:red;"><a href="'+ip+':LIST:'+urlencode(dir)+'">'+img('reload.gif')+nick(ip)+': '+copy(dir,3)+'</a></div>';
    // ������
    if length(levelup(dir))>2 then
      ht:=ht+'<a href="'+ip+':LIST:'+urlencode(levelup(dir))+'">'+img('folderup.gif') + '..' + '</a><br>'+LF
    else
      ht:=ht+'<a href="'+ip+':LIST:\">'+img('folderup.gif') + '\' + '</a><br>'+LF;
    // �������� (�������+1)
    for i := 0 to files.Count-1 do
    begin
      d := GetTag('d',files.strings[i]);
      if (pos(dir, files.strings[i]) <> 0)and(d > '')and(dir<>d) then
        if levelCount(d) = currentLevel + 1 then
        begin
          ht:=ht+'<a href="'+ip+':LIST:'+urlencode(d)+'">'+img('folder.gif') + extractfilename(d) + '</a> ' + ' ' + '<br>'+LF;
        end;
    end;
    ht := ht + '<hr>';
    // �����
    for i := 0 to files.Count-1 do
    begin
      f := GetTag('f',files.strings[i]);
      if (pos(dir, files.strings[i]) <> 0)and(f > '') then
        if levelCount(f)-1 = currentLevel then
        begin
          if isVideoAudio(f) then
            playLink := '<a href="'+ip+':PLAY:'+urlencode(f,true)+'">' + img('play.gif') + '</a> '
          else
            playLink := '';
          ht:=ht+'<a href="'+ip+':GET:'+urlencode(f,true)+'">' + extractfilename(f) + '</a> ' + size2str(toint(GetTag('s',files.strings[i]))) + playLink + '<br>'+LF;
        end;
    end;
  end;
  Result := ht;
  //savetofile(plgpath+'mylist.html', ht);
end;

function GetIP(url:string):string;
var
  ps1, ps2: integer;
begin
  if pos('//',url)=0 then
    url := '//'+url;
  ps1 := pos('//',url);
  ps2 := posex(':',url,ps1+2);
  if (ps1<>0)then
  begin
    if (ps2<>0) then
      result := copy(url, ps1+2, ps2-ps1-2)
    else
      result := copy(url, ps1+2);
  end
  else
    result := url;
end;

function speedToText(speed: real): string;
begin
  Result := FloatToStr(SimpleRoundTo(speed, -1))+' ��/���';
end;

// ������� ������
function Downloads2html:string;
var
  ht: string;
  i: integer;
  clr: string;
  ip: string;
  downloadSek: Real;
  fileSpeed: Real;
  fileSize: Int64;
begin
  for i := 0 to downloads.count -1 do
  begin
    ip := GetIp(downloads.FileInfo[i].url);
    ht := ht + '<a href="icomfile://'+urlencode(Downloaddir+extractfilename(downloads.FileInfo[i].fName))+'">' + nick(ip) +': ' + DelDiskName(downloads.FileInfo[i].fName) + '</a>';
    if downloads.FileInfo[i].Percent < 100 then
    begin
      clr := 'red';
      if downloads.FileInfo[i].Percent < 0 then
        ht := ht + '<a href="cancel://'+urlencode(Downloaddir+extractfilename(downloads.FileInfo[i].fName))+'"> <font color=red> ������ </font></a>'
      else
        ht := ht + ' <font color='+clr+'>' + inttostr(downloads.FileInfo[i].Percent) + '%' + '</font>'+'<a href="cancel://'+urlencode(downloads.FileInfo[i].url, true)+'"> '+img('cancel.gif')+'</a>';
    end
    else if downloads.FileInfo[i].Percent = 999 then
    begin
      ht := ht + '<a href="cancel://'+urlencode(Downloaddir+extractfilename(downloads.FileInfo[i].fName))+'"> <font color=red> '+downloads.FileInfo[i].ResponseText+'</font></a>'
    end
    else
    begin
      clr := 'green';
      ht := ht + ' <font color='+clr+'>' + inttostr(downloads.FileInfo[i].Percent) + '%' + '</font>';
      downloadSek := (downloads.FileInfo[i].lastTime-downloads.FileInfo[i].startTime)/one_sek;
      fileSize := myfileSize(Downloaddir+extractfilename(downloads.FileInfo[i].fName));
      fileSpeed := fileSize/1024/1024/downloadSek;
      ht := ht+'&nbsp;'+speedTotext(filespeed);
    end;
    ht := ht + '<br>';
  end;
  //
  result := HtmlBegin + ht + HtmlEnd;
  Downloads.NotModified;
end;

function Uploads2html:string;
var
  ht: string;
  i: integer;
begin
  for i := 0 to uploads.count -1 do
    ht := ht + Nick(GetIP(uploads.FileInfo[i].url)) + ': <a href="icomfile://'+urlencode(uploads.FileInfo[i].fName)+'">' + DeldiskName(uploads.FileInfo[i].fName) + '</a><br>';
  //
  result := HtmlBegin + ht + HtmlEnd;
  Uploads.NotModified;
end;

procedure CreateFullPath(path:string);
var
  ps:array[1..maxLevel] of integer;
  newPath: string;
  i,j:integer;
begin
  path := ExtractFilePath(path);
  for i := 1 to maxLevel do
    ps[i] := 0;
  j := 1;
  for i := 1 to length(path) do
  begin
    if path[i]='\' then
    begin
      ps[j] := i;
      j := j+1;
    end;
  end;
  for i := 2 to maxLevel do  // ������ ����������
  begin
    if ps[i] = 0 then
      break;
    newPath := Copy(path, 1, ps[i]);
    if not DirectoryExists(newPath) then
      CreateDir(newPath);
  end;
end;

// ������� ������� ����
function LastDir(path:string):string;
var
  ps:array[1..maxLevel] of integer;
  i, j, ps1, ps2:integer;
begin
  path:=ExtractFilePath(path);
  for i := 1 to maxLevel do
    ps[i] := 0;
  j := 1;
  for i := 1 to length(path) do
  begin
    if path[i]='\' then
    begin
      ps[j] := i;
      j := j+1;
    end;
  end;
  ps1 := 0; ps2 := 0;
  for i := maxLevel downto 1 do
  begin
    if ps[i] <> 0 then
    begin
      ps2 := ps[i];
      ps1 := ps[i-1];
      break
    end;
  end;
  if (ps1<>0)and(ps2<>0) then
    result := copy(path, ps1, ps2)
  else
    result := path;
end;

// �������� ������� � ��������
function InternalDir(baseDir, dir:string):string;
var
  ps:integer;
begin
  ps := pos(baseDir, dir);
  if ps <> 0 then
    result := copy(dir, ps+length(baseDir)-1)
  else
    result := dir;
  if result = '\' then
    result := dir;
end;

procedure Exec(url:string);
var
  rz: Windows.HINST;
begin
  rz := ShellExecute(0, 'open', pchar(url), nil, nil, SW_SHOW);
  if (rz = SE_ERR_NOASSOC) or (rz = SE_ERR_ASSOCINCOMPLETE) then
    // ��� ���������� - ����� ���� ������� � �������
    ShellExecute(0, 'open', pchar('rundll32.exe'), pchar('shell32.dll,OpenAs_RunDLL ' + url),
      nil, SW_SHOWNORMAL);
end;

end.
