object ShareSetupForm: TShareSetupForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 403
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 147
    Width = 242
    Height = 237
    Align = alClient
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 242
    Top = 147
    Width = 129
    Height = 237
    Align = alRight
    TabOrder = 1
    object AddBtn: TBitBtn
      Left = 14
      Top = 24
      Width = 99
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 0
      OnClick = AddBtnClick
    end
    object DelBtn: TBitBtn
      Left = 16
      Top = 64
      Width = 97
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 1
      OnClick = DelBtnClick
    end
    object RescanBtn: TBitBtn
      Left = 16
      Top = 144
      Width = 97
      Height = 25
      Caption = #1057#1082#1072#1085#1080#1088#1086#1074#1072#1090#1100
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 2
      OnClick = RescanBtnClick
    end
  end
  object ListBox1: TListBox
    Left = 0
    Top = 147
    Width = 242
    Height = 237
    Align = alClient
    ItemHeight = 13
    Sorted = True
    TabOrder = 2
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 384
    Width = 371
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 127
    Align = alTop
    TabOrder = 4
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 68
      Height = 13
      Caption = #1044#1083#1103' '#1079#1072#1075#1088#1091#1079#1082#1080
    end
    object SpeedButton1: TSpeedButton
      Left = 319
      Top = 11
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 138
      Height = 13
      Caption = 'C'#1082#1086#1088#1086#1089#1090#1100' '#1079#1072#1075#1088#1091#1079#1082#1080', '#1052#1041'/'#1089#1077#1082
    end
    object Label3: TLabel
      Left = 8
      Top = 92
      Width = 130
      Height = 13
      Caption = 'C'#1082#1086#1088#1086#1089#1090#1100' '#1086#1090#1076#1072#1095#1080', '#1052#1041'/'#1089#1077#1082
    end
    object Edit1: TEdit
      Left = 82
      Top = 11
      Width = 231
      Height = 21
      TabOrder = 0
    end
    object SpinEdit1: TSpinEdit
      Left = 152
      Top = 53
      Width = 190
      Height = 22
      MaxValue = 100
      MinValue = 1
      TabOrder = 1
      Value = 1
    end
    object SpinEdit2: TSpinEdit
      Left = 152
      Top = 89
      Width = 190
      Height = 22
      MaxValue = 100
      MinValue = 1
      TabOrder = 2
      Value = 1
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 127
    Width = 371
    Height = 20
    Align = alTop
    Caption = #1044#1086#1089#1090#1091#1087#1085#1099#1077' '#1087#1072#1087#1082#1080
    TabOrder = 5
  end
  object JvBrowseForFolderDialog1: TJvBrowseForFolderDialog
    Left = 64
    Top = 312
  end
end
