unit ithreads;

interface

uses
  Classes, dialogs;

type
  Tdproc = procedure(url, localFile:string) of object;

  TiThread = class(TThread)
  private
    procedure DoVisual;
  protected
  public
    url: string;
    localfile: string;
    dProc:Tdproc;
    constructor Create;
    procedure Execute; override;
  end;

implementation

constructor TiThread.Create;
begin
  FreeOnTerminate := True;
  inherited Create(true);  // true - �� ��������� �����
end;

procedure TiThread.DoVisual;
begin
  showmessage(url);
end;

procedure TiThread.Execute;
begin
  if url = '' then
    Exit;
  if Assigned(dProc) then
    dproc(url, localfile);
  //Synchronize(DoVisual);
end;


end.
