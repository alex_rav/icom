object HttpForm: THttpForm
  Left = 0
  Top = 0
  Caption = 'HttpForm'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object HTTPServer: TIdHTTPServer
    Bindings = <>
    DefaultPort = 6712
    OnConnect = HTTPServerConnect
    OnCommandGet = HTTPServerCommandGet
    Left = 104
    Top = 40
  end
end
