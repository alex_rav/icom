library demo1;

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Jpeg,
  ExtCtrls,
  Buttons,
  api in '..\..\api.pas';

{$R plugin.RES}

const
  hmin=18;  // ����������� ������ ������ ������ �������������
  minisize=30; // ������ ��������
  hh1='h1.html';
  hh2='h2.html';
  hh3='h3.html';
  alpha='ABCDEFGHJIKLMNOPQRSTUVWZYZ';
  re:array[1..2]of string=('1234567890qwertyuiop[]asdfghjkl;''zxcvbnm,./','1234567890��������������������������������.');
  maxmsg=1000;
  myPort=6711;
  OFFLINE=-1;
  OFF=0;
  BUSY=1;
  READY=2;
  ONLINE=2;
  max=50;

type
  Tuser=record
    name:   string;       // ���
    oname:  string;       // ������������ ���
    status: integer;      // ������ -1=������� 0=������� 1=������ 2=�������
    time:   double;       // ����� ���������� �������
    ver:    string;       // ������ �����
    winver: string;       // ������ �����
    ex_status: string;    // ���. ��������� ������
    dr:     string;       // �� ddmmyyyy
    sortorder:integer;    // ��� ���������� �� ������
    mini:   Tjpegimage;   // ���� �������� 30x30
    big:    Tjpegimage;   // �������� 100x100
    winstatus:boolean;    // true=������� false=�������
    idletime:integer;     // ����� �������
    private_mode:boolean; // � ������ "������ ������"
    blink:integer;        // ������ � ������ �����.
    male:integer;         // 0-� 1-�
  end;
  Tusers = array [-1..max] of Tuser;  // ������������ �� 0 �� ���, -1 ��� ������

var
  Plg: IPluginInterface;
  //DA: TApplication;

  pusers:^Tusers; // ��������� �� ������ �������������
  // ������� �� ������ �������
  main_panel:widestring;
  group1:widestring;
  main_btn, btn1, btn2, btn3, btn4:widestring;
  edit1:widestring;
  vr:string;

Function PluginName:widestring; forward;

// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
 PluginName:='demo plugin 1';
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
 PluginVer:='0.1';
end;

// ������������ �������� ��� �������� dll  (������������)
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
    // Application:=DA;
  end;
end;

// ������� ������
Procedure PluginClick;
var
  ss:string;
begin
 plg.InsertText('������ Add by plugins');
 ss:='htmldir='+plg.getvar('htmldir');
 showmessage('������ ������ �������'#13+ss);
end;

// �������� �� ������ �� ������
Procedure knopka1click(sender:tobject);
begin
 showmessage('������ ������1');
end;

// �������� �� ������ �� ������
Procedure knopka2click(sender:tobject);
begin
 showmessage('������ ������2');
end;

// �������� �� ������ �� ������
Procedure knopka3click(sender:tobject);
begin
 showmessage('������ ������3');
end;

// �������� �� ������ �� ������
Procedure knopka4click(sender:tobject);
begin
 showmessage('������ ������4');
end;

// ������������� ������� ���������� ��� �������� (������������)
Function Init(app:Tapplication; ppUsers:pointer):boolean;
var
  bm:Tbitmap;
  fl:string;
  path:string;
  i:integer;
  ResStream:TResourceStream;
  FileStream :TFileStream;
  rz:boolean;
begin
 rz:=false;
 try
   try
     // �������������
     //DA:=Application;
     //Application := App;
     // �������� ���������
     if not App.MainForm.GetInterface(IPluginInterface,Plg) then
     begin
        ShowMessage('�� ������� �������� ������ �� ���������!');
        result:=false;
        exit;
     end;
     // ����� �������
     path:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
     CreateDir(path);
     // ������ ������������� (������ ������!)
     pusers:=ppUsers;
     //pusers^[-1].name; // ��������� � ������ �������������
     // ������ ��������
     vr:=plg.getvar('ver');
     // ����������� �������
      for i:=0 to 4 do
      begin
        fl:=path+'b'+inttostr(i)+'.bmp';
        ResStream := TResourceStream.Create(hinstance, widestring('B'+inttostr(i)), 'MY');
        try
           try
            FileStream := TFileStream.Create(fl, fmCreate);
            try
              FileStream.CopyFrom(ResStream, 0);
            finally
              FileStream.Free;
            end;
           except
           end;
        finally
            ResStream.Free;
        end;
      end;

     // ������ �������
     main_btn:=plg.plugin_button(PluginName);
     plg.SetGlyphFile(PluginName,main_btn,widestring(path+'b0.bmp'));
     freeandnil(bm);
     plg.SetOnclick(PluginName, main_btn, addr(PluginClick));
     // ������ �������
     main_panel:=Plg.create_panel(PluginName);
     //if main_panel=nil then exit;
     plg.setwindow(PluginName,main_panel, -1, -1, -1, 50);
     // ������ ������
     group1:=plg.Create_Control(PluginName,'tgroupbox',main_panel);
     plg.SetCaption(PluginName,group1,'������ 1');
     plg.SetAlign(PluginName,group1, alClient);
     // � ������ ���� � ������
     edit1:=plg.Create_Control(PluginName,'tedit',group1);
     plg.setwindow(PluginName,edit1, 10, 14, 100, 24);
     plg.SetText(PluginName,edit1,'�����');
     // ������ 1
     btn1:=plg.create_Control(PluginName,'tspeedbutton',group1);
     plg.setwindow(PluginName,btn1, 120, 14, 22, 22);          // ������� � ���������
     plg.SetGlyphFile(PluginName,btn1,widestring(path+'b1.bmp'));   // �������� �� ������ 1
     plg.SetOnclick(PluginName, btn1, addr(knopka1click));     // ���������� ������ 1
     // ������ 2
     btn2:=plg.create_Control(PluginName,'tspeedbutton',group1);
     plg.setwindow(PluginName,btn2, 144, 14, 22, 22);
     plg.SetGlyphFile(PluginName,btn2,widestring(path+'b2.bmp'));
     plg.SetOnclick(PluginName, btn2, addr(knopka2click));
     // ������ 3
     btn3:=plg.create_Control(PluginName,'tspeedbutton',group1);
     plg.setwindow(PluginName,btn3, 168, 14, 22, 22);          // ������� � ���������
     plg.SetGlyphFile(PluginName,btn3,widestring(path+'b3.bmp'));   // �������� �� ������ 1
     plg.SetOnclick(PluginName, btn3, addr(knopka3click));     // ���������� ������ 1
     // ������ 4
     btn4:=plg.create_Control(PluginName,'tspeedbutton',group1);
     plg.setwindow(PluginName,btn4, 181, 14, 22, 22);          // ������� � ���������
     plg.SetGlyphFile(PluginName,btn4,widestring(path+'b4.bmp'));   // �������� �� ������ 1
     plg.SetOnclick(PluginName, btn4, addr(knopka4click));     // ���������� ������ 1
     // �����
     rz:=true;
   except
     rz:=false;
   end;
 finally
   result:=rz;
 end;
end;

{���������� ������� �������: CTRL/ALT/WIN+CTRL/ALT/WIN+�����/�����
 ps/���� ��������� �� �������}
Procedure GetHotKey(var hot1, hot2, hot3:widestring);
begin
 hot1:='WIN';
 hot2:='ALT';
 hot3:='D';
end;

{�������� �� ������� ������� ������}
Procedure HotKey;
begin
  showmessage('Hot key');
end;


{����� ����� �������� �������}
Procedure Setup;
begin
 showmessage('Setup click');
end;
{
FromName - ��� ������������ (_sys-��������� ���������, _err-��������� �� ������)
ch - ����� ������ 0-������� 1-����� 2-������� >2 ������ 98-��������� 99-������
msg - ��������� � ���� html
}
Procedure BeforeMessage(FromName:widestring; ch:integer; var msg:widestring);
begin
 msg:=widestring(msg+'<br> add by plugin');
end;

{
FromName - ��� ������������ (_sys-��������� ���������, _err-��������� �� ������)
ch - ����� ������ 0-������� 1-����� 2-������� >2 ������ 98-��������� 99-������
msg - ��������� � ���� html
}
Procedure BeforeSysMessage(FromName:widestring; ch:integer; var msg:widestring);
begin
 msg:=widestring(msg+' sys add by plugin');
end;



{
// ���������� �������� �� ������ �������
Function PluginImage:pointer;
var
  bm:Tbitmap;
  st:tmemorystream;
begin
  bm:=Tbitmap.Create;
  try
    bm.transparent:=true;
    form1.ImageList2.GetBitmap(0,bm);
    st:=tmemorystream.Create;
    bm.SaveToStream(st);
    st.position:=0;
    result:=st;
  except
    result:=nil;
  end;
end;
}
// �������� ID �� �����
Function GetID(name:string):integer;
var
 i:integer;
begin
 name:=trim(name);
 result:=-1;
 if name='' then
    result:=-1
 else
 for i:=0 to max do
 begin
     if pusers^[i].name=name then
     begin
        result:=i;
        break;
     end;
 end;
end;

// ������ ������, ���������� ������������� ����� ���������� ������ (����)
procedure userMeasureItem(Control: TWinControl; Index: Integer; var Height: Integer);
var
 index2:integer;  // ������ � ������ user_list
begin
 index2:=getID(tlistbox(control).Items[Index]);
 if index2<0 then  // ���������
    Height:=hmin
 else if pusers^[index2].status=OFFLINE then
    Height:=hmin
 else
    Height:=minisize;
end;

// ��������� ������ ������������� (����)
procedure userDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
 clname:array[-1..2] of Tcolor=(clGray, clRed, clOlive, clGreen);
var
 bt:Tbitmap;
 foncolor:Tcolor;
 deltax,deltay:integer;
 index2:integer;  // ������ � ������ user_list
begin
 bt:=Tbitmap.Create;
 index2:=getID(Tlistbox(control).Items[Index]);
try
 if index2<0 then exit;
 bt.Transparent:=false;
 foncolor:= Tlistbox(control).Canvas.Pixels[Control.clientwidth-1, Control.clientheight-1]; // ���� ���� �� ������ ������ ����� ���� ��������� (��� ������ ���������)
 With Tlistbox(control) Do
 begin
    canvas.Font.Color := clName[pusers^[index2].status];
    if Tlistbox(control).selected[index] then
    begin
       canvas.Brush.color:=clNavy;
       canvas.CopyMode:=cmSrcCopy;
    end
    else
    begin
       canvas.Brush.Color := foncolor;
       canvas.CopyMode:=cmSrcCopy;
    end;
    canvas.FillRect(Rect);
    // ������-0 ������-1 ������_�����-2 ������_������-3 ������_�����+������_������-4
    if (integer(plg.getvar('user_box_mode')) in [1,4]) then
    begin
       // ������
       //statuslist.GetBitmap(st,bt); ��� ���� ������ ������ �����
       bt.assign(pusers^[index2].mini);
       bt.Height:=16;
       bt.Width:=16;
       canvas.Draw(Rect.Left,Rect.Top,Bt);
    end;
    if (integer(plg.getvar('user_box_mode')) in [2,3,4]) then
    begin
       // ����������
       if (pusers^[index2].mini<>nil)and(not pusers^[index2].mini.Empty)and(pusers^[index2].status<>OFFLINE) then
       begin
          bt.Assign(pusers^[index2].mini);
          if integer(plg.getvar('user_box_mode')) in [3,4] then
             canvas.Draw(rect.Right-bt.Width-2,Rect.Top,Bt)
          else
             canvas.Draw(rect.left+1,Rect.Top,Bt);
       end;
    end;
 end;
finally
 with TListbox(Control) do
 begin
    // ���
    if selected[index] then
    begin
       canvas.Brush.color:=clNavy;
       canvas.CopyMode:=cmSrcCopy;
    end
    else
    begin
       canvas.Brush.Color := plg.getvar('wincolor');
       canvas.CopyMode:=cmMergeCopy;
    end;
    if pusers^[index2].private_mode then
       deltax:=20
    else
       deltax:=0;
    // ������� � ������
    case pusers^[index2].blink of
      1: begin
         canvas.Brush.color:=clNavy;
         pusers^[index2].blink:=2;
         end;
      2: begin
         canvas.Brush.color:= plg.getvar('wincolor');
         pusers^[index2].blink:=1;
         end;
    end;
    case plg.getvar('user_box_mode') of
       0,3: canvas.TextOut(Rect.Left+5+deltax, Rect.Top+2+deltaY, TListbox(Control).Items[Index]);
       1: canvas.TextOut(Rect.Left+18, Rect.Top+2+deltaY, TListbox(Control).Items[Index]);
       2: canvas.TextOut(Rect.Left+minisize+2, Rect.Top+2+deltaY, TListbox(Control).Items[Index]);
       4: canvas.TextOut(Rect.Left+18, Rect.Top+2+deltaY, TListbox(Control).Items[Index]);
       else
          canvas.TextOut(Rect.Left+18, Rect.Top+2+deltaY, TListbox(Control).Items[Index]);
    end;
    // ������� �� �������
    if plg.getvar('drawlines') then
    begin
       canvas.Brush.color:= plg.getvar('wincolor');
       canvas.Pen.Color:=clBtnFace; canvas.Pen.Style:=psDot;
       canvas.MoveTo(rect.Left,rect.Bottom-1); canvas.LineTo(rect.Right,rect.Bottom-1);
    end;
 end;
 bt.free;
end;
end;

exports
 Init,
 PluginName,
 PluginVer,
 BeforeMessage,
 BeforeSysMessage,
 GetHotKey,
 HotKey,
 Setup;
begin
  { ���-������ ���, ��� ������ ����������� �
    �������� ������������� ���������� }
  DLLProc := @MyDLLProc;
end.
