library icom_update;

uses
  windows,
  forms,
  dialogs,
  SysUtils,
  Classes,
  inifiles,
  ExtCtrls,
  IdCoder,
  IdCoderMIME,
  api in '..\..\api.pas',
  VERS in '..\..\VERS.PAS',
  userslist in '..\..\userslist.pas',
  Global in '..\..\Global.pas';

{$R *.res}

var
  Plg: IPluginInterface;
  users: TUsers;

Procedure pluginclick(sender:tobject); forward;

procedure LoadUsers;
var
  us: TstringList;
  i: integer;
  uu, ip: string;
  ps: integer;
  ss: WideString;
begin
  if not assigned(users) then
    users := TUsers.Create
  else
    users.ClearUsers;
  us := TstringList.Create;
  ss := plg.GetUserList;
  us.Text := ss;
  for i := 0 to us.Count-1 do
  begin
    ps := pos('/',us.strings[i]);
    uu := copy(us.strings[i],1, ps-1);
    ip := copy(us.strings[i], ps+1, length(us.strings[i])-ps);
    users.AddUser(uu, ip);
    users[users.Count - 1].name := uu;
    users[users.Count - 1].ip := ip;
    users[users.Count - 1].plugins := plg.GetUserPlugins(ip);
  end;
  // ������� ������
  for i := 0 to us.Count-1 do
    users[i].Status := plg.GetUserStatus(users[i].ip);
end;

function DllName:string;
var
 TheFileName:array[0..MAX_PATH] of char;
begin
 FillChar(TheFileName, sizeof(TheFileName), #0);
 GetModuleFileName(hInstance, TheFileName, sizeof(TheFileName));
 result:=string(TheFileName);
end;

// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
 PluginName:='icom update';
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
 PluginVer:='1.0';
end;

// ������������ �������� ��� �������� dll
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
  end;
end;

// ������������� ������� ���������� ��� �������� (������������)
Function Init(app:Tapplication):boolean;
var
  rz:boolean;
begin
 rz:=false;
 try
   try
     // �������� ���������
     if not App.MainForm.GetInterface(IPluginInterface,Plg) then
     begin
        ShowMessage('�� ������� �������� ������ �� ���������!');
        result:=false;
        exit;
     end;
     Plg.addToPopup(PluginName,'PopupUserlist','-', nil);
     Plg.addToPopup(PluginName,'PopupUserlist','��������� ����������', addr(pluginclick));
     // �����
     rz:=true;
   except
     rz:=false;
   end;
 finally
   result:=rz;
 end;
end;

function MyFileSize(FileName: string): Longint;
var
  SearchRec: TSearchRec;
begin
  if FindFirst(ExpandFileName(FileName), faAnyFile, SearchRec) = 0 then
    result := SearchRec.Size
  else
    result := -1;
  sysutils.FindClose(SearchRec);
end;

// ������� �������� ������������������ ������� � �����
function ToHex(Value: integer): string;
var
  stb, mlb: integer;
const
  hex = '0123456789ABCDEF';
begin
  stb := Value div 16;
  mlb := Value - 16 * stb;
  result := hex[stb + 1] + hex[mlb + 1];
end;

function urlEncode(Value: string; Force: boolean = false): string;
var
  i: integer;
  CH: ansichar;
  ss: string;
  aValue: ansistring;
begin
 //result := urlEncode2(Value, force);
// Exit;
  aValue := ansistring(Value);
  ss := '';
  if length(aValue) <> 0 then
    for i := 1 to length(aValue) do
    begin
      CH := aValue[i];
      if Ord(CH) >= 128 then // �������
      begin
        ss := ss + '%' + ToHex(Ord(byte(CH)));
      end
      else if Ord(CH) = $20 then
        ss := ss + '+'
      else if Force then // ��� ������� ��������������
        ss := ss + '%' + ToHex(Ord(CH))
      else
        ss := ss + char(CH);
    end;
  result := ss;
end;

function Base64Encode(const Text: String): String;
var
  Encoder: TIdEncoderMime;
begin
  Encoder := TIdEncoderMime.Create(nil);
  try
    result := Encoder.EncodeString(Text, TEncoding.ANSI);
  finally
    FreeAndNil(Encoder);
  end
end;

Procedure EncodeV2(var tmpstr: string; sign: string; data: string; Channel, users: integer; file_name, channel_name: string);
const
  LF=#13#10;
var
 fs: int64;
begin
  if fileexists(file_name) then
    fs := myFileSize(file_name)
  else
    fs := 0;
  tmpstr := sign + LF + LF +
    '#CHANNEL_NAME' + LF + urlencode(channel_name) + LF +
    '#CHANNEL' + LF + inttostr(Channel) + LF +
    '#FILE' + LF + urlencode(extractfilename(file_name)) + LF +
    '#USERS' + LF + inttostr(users) + LF +
    '#FROM' + LF + 'ip' + LF +
    '#MSG' + LF + Base64Encode(data) + LF +
    '#FILESIZE' + LF + inttostr(fs) + LF +
    // ����� ������ ��������� ����
    '#END';
end;


// �������� ���� �� ���������� ip
Procedure SendUpdate(ip: string; fname: string);
var
  MyStream: TMemoryStream;
  SStream: TStringStream;
  fileStr: string;
  tmpstr: string;
  file_name: string;
begin
  if not fileexists(fname) then
    exit;
  LoadUsers;
  // �������� �����
  MyStream := TMemoryStream.Create;
  SStream := TStringStream.Create;
  try
    try
      MyStream.LoadFromFile(fname);
      if MyStream.Size = 0 then
        exit;
      MyStream.Position:=0;
      SStream.CopyFrom(MyStream, MyStream.Size);
      fileStr := SStream.DataString;
      // �������� ������
      file_name := extractfilename(fname);
      EncodeV2(tmpstr, '#UPDATEDATA', fileStr, 0, 0, urlencode(file_name), '');
      Plg.TCPSend(ip, tmpstr);
      Plg.icomlog('plgSendupdate ' + fname + ' ' + users.GetNickName(ip));
      Plg.addSimpleMessage('update '+users.GetNickName(ip)+' '+extractfilename(fname)+'<br>');
    except
      on e: exception do
        Plg.addSimpleMessage('>'+extractfilename(fname)+' '+e.Message+'<br>');
    end;
  finally
    FreeAndNil(MyStream);
    FreeAndNil(SStream);
  end;
end;

// �������� �� ������ �� ������
Procedure pluginclick(sender:tobject);
var
  ip: WideString;
  sr:tsearchrec;
begin
  try
    ip := Plg.GetSelectedUser;
    if ip>'' then
      SendUpdate(ip, extractfilepath(application.exename)+'icom.exe');
    if FindFirst(extractfilepath(application.exename)+'plugins\*.dll', faAnyFile, sr)=0 then
    begin     
      repeat
        if pos(UpperCase(sr.name),UpperCase(DllName))=0 then
          SendUpdate(ip, extractfilepath(application.exename)+'plugins\'+sr.name);
      until FindNext(sr) <> 0;
    end;
  except
    on e: Exception do
      Plg.icomlog('plgSendupdate ' + e.Message);
  end;
end;

exports
 Init,
 PluginName,
 PluginVer;
begin
  { ���-������ ���, ��� ������ ����������� �
    �������� ������������� ���������� }
  DLLProc := @MyDLLProc;
end.

