unit snimok_setup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sLabel, sComboBox;

type
  TSetupForm = class(TForm)
    Edit_snap: TEdit;
    Combo_snap: TsComboBox;
    sLabel1: TsLabel;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    procedure Edit_snapClick(Sender: TObject);
    procedure Edit_snapKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SetupForm: TSetupForm;

implementation

{$R *.dfm}

procedure TSetupForm.Edit_snapClick(Sender: TObject);
begin
 LoadKeyboardLayout('00000409', KLF_ACTIVATE); //����
end;

procedure TSetupForm.Edit_snapKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F1 then (sender as tedit).text:='F1'
  else if key=VK_F2 then (sender as tedit).text:='F2'
  else if key=VK_F3 then (sender as tedit).text:='F3'
  else if key=VK_F4 then (sender as tedit).text:='F4'
  else if key=VK_F5 then (sender as tedit).text:='F5'
  else if key=VK_F6 then (sender as tedit).text:='F6'
  else if key=VK_F7 then (sender as tedit).text:='F7'
  else if key=VK_F8 then (sender as tedit).text:='F8'
  else if key=VK_F9 then (sender as tedit).text:='F9'
  else if key=VK_F10 then (sender as tedit).text:='F10'
  else if key=VK_F11 then (sender as tedit).text:='F11'
  else if key=VK_F12 then (sender as tedit).text:='F12'
  else if key=VK_LEFT then (sender as tedit).text:='LEFT'
  else if key=VK_RIGHT then (sender as tedit).text:='RIGHT'
  else if key=VK_UP then (sender as tedit).text:='UP'
  else if key=VK_DOWN then (sender as tedit).text:='DOWN'
  else if key=VK_SPACE then (sender as tedit).text:='SPACE'
  else if key=VK_SNAPSHOT then (sender as tedit).text:='PRINT'
  else
  if (key>ord(' ')) and (key<ord('Z')) then
  begin
       key:=0;
       (sender as tedit).text:=chr(key);
  end
  else
  begin
       key:=0;
       (sender as tedit).text:='';
  end;
end;

end.
