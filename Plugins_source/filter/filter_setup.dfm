object SetupForm: TSetupForm
  Left = 417
  Top = 242
  Width = 265
  Height = 153
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object sLabel1: TsLabel
    Left = 56
    Top = 8
    Width = 111
    Height = 16
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1101#1082#1088#1072#1085
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object Edit_snap: TEdit
    Left = 144
    Top = 32
    Width = 49
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 1
    TabOrder = 0
    OnClick = Edit_snapClick
    OnEnter = Edit_snapClick
    OnKeyDown = Edit_snapKeyDown
  end
  object Combo_snap: TsComboBox
    Left = 56
    Top = 34
    Width = 81
    Height = 19
    Alignment = taLeftJustify
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'COMBOBOX'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ItemIndex = 0
    ParentFont = False
    TabOrder = 1
    Text = 'CTRL'
    Items.Strings = (
      'CTRL'
      'ALT'
      'WIN')
  end
  object sBitBtn1: TsBitBtn
    Left = 40
    Top = 88
    Width = 75
    Height = 25
    Hint = #1055#1086#1089#1083#1077' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1090#1088#1077#1073#1091#1077#1090#1089#1103' '#1088#1077#1089#1090#1072#1088#1090' '#1080#1082#1086#1084#1072
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Kind = bkOK
    SkinData.SkinSection = 'BUTTON'
  end
  object sBitBtn2: TsBitBtn
    Left = 136
    Top = 88
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkCancel
    SkinData.SkinSection = 'BUTTON'
  end
end
