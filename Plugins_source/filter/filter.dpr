library filter;

uses
  SysUtils,
  Classes,
  windows,
  forms,
  inifiles,
  controls,
  filter_main in 'filter_main.pas',
  api in '..\..\api.pas',
  filter_setup in 'filter_setup.pas';

{$R *.res}
var
  Plg: IPluginInterface;
  MainApp: TApplication;
  h1,h2,h3:string;
  path:string;

Function PluginName:pchar; forward;

// ���������� ��� �������� (������������)
Function PluginName:pchar;
begin
 PluginName:='Filter plugin';
end;

// ���������� ������ �������� (������������)
Function PluginVer:pchar;
begin
 PluginVer:='1.0';
end;

// ������������ �������� ��� �������� dll  (������������)
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
    FreeAndNil(fmSnimok);
  end;
end;

// �������� ���������
procedure LoadSetup;
var
  fl:Tinifile;
begin
  fl:=Tinifile.Create(path+PluginName+'.ini');
  try
    h1:=fl.ReadString('hotkey','h1','WIN');
    h2:=fl.ReadString('hotkey','h2','');
    h3:=fl.ReadString('hotkey','h3','P');
  finally
    fl.free;
  end;
end;

// �������� ���������
procedure SaveSetup;
var
  fl:Tinifile;
begin
  fl:=Tinifile.Create(path+PluginName+'.ini');
  try
    fl.WriteString('hotkey','h1',h1);
    fl.WriteString('hotkey','h2',h2);
    fl.WriteString('hotkey','h3',h3);
  finally
    fl.free;
  end;
end;

// ������������� ������� ���������� ��� �������� (������������)
function Init(app:Tapplication; ppUsers, ppMemoin:pointer):boolean;
begin
 result:=false;
 // �������������
 MainApp:=app;
 // �������� ���������
 if not App.MainForm.GetInterface(IPluginInterface,Plg) then
 begin
    MessageBox(0, '�� ������� �������� ������ �� ���������!', 'Error', mb_ok);
    exit;
 end;
 // ��������� �� �����
 snimok_main.Plg:=plg;
 snimok_main.MainApp:=app;
 // ����� �������
 path:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
 CreateDir(path);
 // ���������
 loadsetup;
 result:=true;
end;

{���������� ������� �������: CTRL/ALT/WIN+CTRL/ALT/WIN+�����/�����
 ps/���� ��������� �� �������}
Procedure GetHotKey(var hot1, hot2, hot3:pchar);
begin
 hot1:=pchar(h1);
 hot2:=pchar(h2);
 hot3:=pchar(h3);
end;

{�������� �� ������� ������� ������}
Procedure HotKey;
begin
 if fmSnimok=nil then
    fmsnimok:=TfmSnimok.Create(Application);
 GetScreen;
 MainApp.MainForm.Show;
 MainApp.MainForm.BringToFront;
 fmsnimok.Show;
 fmsnimok.BringToFront;
end;

{����� ����� �������� �������}
Procedure Setup;
begin
 if SetupForm=nil then
    SetupForm:=TSetupForm.Create(Application);
 SetupForm.Combo_snap.Text:=h1;
 SetupForm.Edit_snap.Text:=h3;
 SetupForm.ShowModal;
 if SetupForm.ModalResult=mrok then
 begin
   h1:=SetupForm.Combo_snap.Text;
   h3:=SetupForm.Edit_snap.Text;
   SaveSetup;
 end;
end;

{
FromName - ��� ������������ (_sys-��������� ���������, _err-��������� �� ������)
ch - ����� ������ 0-������� 1-����� 2-������� >2 ������ 98-��������� 99-������
msg - ��������� � ���� html
}
Procedure BeforeMessage(FromName:pchar; ch:integer; var msg:pchar);
begin
 msg:=pchar(msg+'<br> add by plugin');
end;

{
FromName - ��� ������������ (_sys-��������� ���������, _err-��������� �� ������)
ch - ����� ������ 0-������� 1-����� 2-������� >2 ������ 98-��������� 99-������
msg - ��������� � ���� html
}
Procedure BeforeSysMessage(FromName:pchar; ch:integer; var msg:pchar);
begin
 msg:=pchar(msg+' sys add by plugin');
end;

exports
 Init,
 PluginName,
 PluginVer,
 GetHotKey,
 HotKey,
 Setup,
 BeforeMessage,
 BeforeSysMessage;

begin
  DLLProc := @MyDLLProc;
end.
