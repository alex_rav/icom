library gismeteo;

uses
  Windows,
  SysUtils,
  Classes,
  Forms,
  Dialogs,
  extCtrls,
  api in '..\..\api.pas',
  vers in '..\..\VERS.PAS';

{$R plugin.RES}

var
  Plg: IPluginInterface;
  // ������� �� ������ �������
  main_panel:widestring;
  main_btn:widestring;
  //



// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
 Result:='GISmeteo';
end;

function DllName:string;
var
 TheFileName:array[0..MAX_PATH] of char;
begin
 FillChar(TheFileName, sizeof(TheFileName), #0);
 GetModuleFileName(hInstance, TheFileName, sizeof(TheFileName));
 result:=string(TheFileName);
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
 PluginVer:='1.7';
end;

// ������������ �������� ��� �������� dll
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
    ;
  end;
end;

function pagebegin:string;
begin
    result :=
    '<!DOCTYPE html>'+
    '<html>'+
    '<head>'+
    '	<title></title>'+
    '	<meta http-equiv="content-type" content="text/html; charset=windows-1251">'+
    '</head>'+
    '<body>';
end;
function pageend:string;
begin
  result := '</body></html>';
end;

// ������� ������
procedure CutPage(page:string);
var
  ps:integer;
  txt:tstringlist;
begin
  if fileexists(page) then
  begin
    txt:=tstringlist.Create;
    txt.LoadFromFile(page);
    ps:=pos('<!--TYPO3SEARCH_begin-->', txt.text);
    if ps<>0 then
    begin
       txt.text:=copy(txt.text,ps, length(txt.text)-ps+1);
    end;
    ps:=pos('<p align="right">��������: www.gismeteo.ru</p>', txt.text);
    if ps<>0 then
    begin
       txt.text:=copy(txt.text,1, ps-1);
    end;
    txt.Text:=PageBegin + stringreplace(txt.Text, 'href="index', 'href="http://nkmzinfo/index', [rfReplaceAll]) + PageEnd;
    txt.SaveToFile(page);
  end;

end;

// �������� �� ������ �� ������
Procedure pluginclick(sender:tobject);
var
  page:string;
begin
  page:=plg.DownloadLocal('http://nkmzinfo/index.php?id=392&town=Kramatorsk');
  if page='' then exit;
  cutpage(page);
  plg.ShowPage('������ ����������', widestring(page));
end;

// ����������� �������
Procedure ExtractRes(path:string);
var
  fl:string;
  i:integer;
  ResStream:TResourceStream;
  FileStream :TFileStream;
begin
  for i:=1 to 1 do
  begin
    fl:=path+'b'+inttostr(i)+'.bmp';
    ResStream := TResourceStream.Create(hinstance, widestring('B'+inttostr(i)), 'MY');
    try
       try
        FileStream := TFileStream.Create(fl, fmCreate);
        try
          FileStream.CopyFrom(ResStream, 0);
        finally
          FileStream.Free;
        end;
       except
       end;
    finally
        ResStream.Free;
    end;
  end;
end;


// ������������� ������� ���������� ��� �������� (������������)
Function Init(app:Tapplication):boolean;
var
//  bm:Tbitmap;
  path:string;
  rz:boolean;
begin
 rz:=false; 
 try
   try
     // �������� ���������
     if not App.MainForm.GetInterface(IPluginInterface,Plg) then
     begin
        ShowMessage('�� ������� �������� ������ �� ���������!');
        result:=false;
        exit;
     end;
     // ����� �������
     path:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
     CreateDir(path);
     // ����������� �������
     ExtractRes(path);
     // ������ �������
     main_btn:=plg.plugin_button(PluginName);
     plg.SetGlyphFile(PluginName,main_btn,widestring(path+'b1.bmp'));
     plg.SetOnclick(PluginName, main_btn, addr(PluginClick));
     // �����
     rz:=true;
   except
     rz:=false;
   end;
 finally
   result:=rz;
 end;
end;

{����� ����� �������� �������}
Procedure Setup;
begin
 showmessage('Setup click');
end;

exports
 Init,
 PluginName,
 PluginVer;
begin
  { ���-������ ���, ��� ������ ����������� �
    �������� ������������� ���������� }
  DLLProc := @MyDLLProc;
end.
