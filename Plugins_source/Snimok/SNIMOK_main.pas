unit SNIMOK_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  clipbrd,
  jpeg,
  api,
  StdCtrls, Buttons, ExtCtrls,
  Dialogs,
  ExtDlgs;

type
  TfmSnimok = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    CutButton: TSpeedButton;
    SaveButton: TSpeedButton;
    Image1: TImage;
    CopyButton: TSpeedButton;
    DelButton: TSpeedButton;
    LeftRot: TSpeedButton;
    rightRot: TSpeedButton;
    BitBtn2: TBitBtn;
    SavePictureDialog1: TSavePictureDialog;
    procedure FormActivate(Sender: TObject);
    procedure CutButtonClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CopyButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure LeftRotClick(Sender: TObject);
    procedure rightRotClick(Sender: TObject);
    procedure DelButtonClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    Capturing : bool;
    Captured : bool;
    StartPlace : TPoint;
    EndPlace : TPoint;
    kfx, kfy: real;
  public
    { Public declarations }
  end;


var
 path:string;
 fmSnimok: TfmSnimok;
 pmemoin:pointer;

Procedure GetScreen;

implementation


{$R *.DFM}

uses global;


// ������� ������ ������� ��������
Procedure SetStretch;
begin
 with fmSnimok do
 begin
 if image1.Picture.Width<image1.Width then
    image1.Stretch:=false
 else
    image1.Stretch:=true;
 image1.Refresh;
 end;
end;


function MakeRect(Pt1: TPoint; Pt2: TPoint): TRect;
begin
  if pt1.x < pt2.x then
  begin
    Result.Left := pt1.x;
    Result.Right := pt2.x;
  end
  else
  begin
    Result.Left := pt2.x;
    Result.Right := pt1.x;
  end;
  if pt1.y < pt2.y then
  begin
    Result.Top := pt1.y;
    Result.Bottom := pt2.y;
  end
  else
  begin
    Result.Top := pt2.y;
    Result.Bottom := pt1.y;
  end;
end;


Procedure GetScreen;
var
 DeskTopDC: HDc;
 DeskTopCanvas: TCanvas;
 TempImage:Timage;
 JpegImage:Tjpegimage;
begin
 path:=extractfilepath(application.exename);
 TempImage := TImage.Create(NIL);
 With TempImage do
 begin
  Height := Screen.Height;
  Width := Screen.Width;
 end;
 JpegImage := TJpegImage.Create;
 // ������� ���� ����� � TempImage
 TempImage.Canvas.copymode := cmSrcCopy;
 DeskTopDC := GetWindowDC(GetDeskTopWindow);
 DeskTopCanvas := TCanvas.Create;
 DeskTopCanvas.Handle := DeskTopDC;
 TempImage.Canvas.CopyRect(TempImage.Canvas.ClipRect, DeskTopCanvas, DeskTopCanvas.ClipRect);
 fmSnimok.Image1.Picture.Assign(TempImage.Picture);
// fmSnimok.Image1.Picture.SaveToFile('screen.bmp');

 JpegImage.Assign(TempImage.Picture.Bitmap);
 JpegImage.CompressionQuality:=90;
 JpegImage.Compress;
 JpegImage.SaveToFile(path+'screen.jpg');

 TempImage.Free;
 JpegImage.Free;
end;

procedure TfmSnimok.FormActivate(Sender: TObject);
var
 r : TRect;
begin
 fmSnimok.DoubleBuffered:=true;
 // ����� � ������ �������� �����
 SystemParametersInfo(SPI_GETWORKAREA,0,@r,0);
 fmSnimok.Top:=0; fmSnimok.Left:=0;
 fmSnimok.Width:=r.Right; fmSnimok.Height:=r.Bottom;
 fmSnimok.Color:=clgreen;
 startPlace.x:=0; StartPlace.y:=0; EndPlace.x:=0; EndPlace.y:=0;
 SetStretch;
end;

procedure TfmSnimok.CutButtonClick(Sender: TObject);
var
 copyRect:Trect;
 TempImage:Timage;
begin
 DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
 image1.refresh;
 // ������� ��� �����������
 copyRect:=MakeRect(StartPlace,EndPlace);
 // ��������
 TempImage := TImage.Create(NIL);
 TempImage.width:=abs(copyRect.Right-copyRect.Left);
 TempImage.height:=abs(copyRect.bottom-copyRect.top);
 TempImage.Canvas.CopyRect(TempImage.Canvas.ClipRect, Image1.Canvas, copyRect);
 Image1.picture.assign(TempImage.Picture);
 TempImage.Free;
 startPlace.x:=0; StartPlace.y:=0; EndPlace.x:=0; EndPlace.y:=0;
 SetStretch;
 fmSnimok.Color:=clgreen;
end;

procedure TfmSnimok.BitBtn1Click(Sender: TObject);
begin
 fmSnimok.close;
end;

procedure TfmSnimok.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if image1.Stretch then
 begin
  kfx:=image1.picture.Width/image1.Width;
  kfy:=image1.Picture.Height/image1.Height;
 end
 else
 begin
  if (x>image1.picture.Width) or (y>image1.picture.height) then
  begin
     messagebeep(mb_ok);
     exit;
  end;
  kfx:=1; kfy:=1;
 end;
  if Captured then
     DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace, EndPlace))
  else
  begin
     startPlace.x:=0; StartPlace.y:=0; EndPlace.x:=0; EndPlace.y:=0;
     DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace, EndPlace));
  end;
  StartPlace.x := trunc(X*kfx);
  StartPlace.y := trunc(Y*kfy);
  EndPlace.x := trunc(X*kfx);
  EndPlace.y := trunc(Y*kfy);
  DrawFocusRect(image1.Canvas.Handle, MakeRect(StartPlace, EndPlace));
  Capturing := true;
  Captured := true;
end;

procedure TfmSnimok.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if Capturing then
  begin
    DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
    if (x>image1.picture.Width) then
    begin
        messagebeep(mb_ok);
        EndPlace.x := image1.picture.Width;
    end
    else
    begin
        EndPlace.x := trunc(X*kfx);
    end;
    if (y>image1.picture.Height) then
    begin
        messagebeep(mb_ok);
        EndPlace.y := image1.picture.Height;
    end
    else
    begin
        EndPlace.y := trunc(Y*kfy);
    end;
    DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
    Image1.refresh;
  end
end;

procedure TfmSnimok.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Capturing := false;
end;

procedure TfmSnimok.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
end;

procedure TfmSnimok.CopyButtonClick(Sender: TObject);
begin
 DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
 image1.refresh;
 Clipboard.Assign(image1.Picture.Bitmap);
end;


procedure TfmSnimok.SaveButtonClick(Sender: TObject);
var
 Jp: Tjpegimage;
begin
 if not SavePictureDialog1.Execute then exit;
 DrawFocusRect(image1.Canvas.Handle,MakeRect(StartPlace,EndPlace));
 image1.refresh;
 Jp:=Tjpegimage.create;
 Jp.Assign(fmSnimok.image1.picture.bitmap);
 Jp.CompressionQuality:=90;
 Jp.Compress;
 Jp.SaveToFile(SavePictureDialog1.FileName);
 Jp.Free;
end;

procedure TfmSnimok.LeftRotClick(Sender: TObject);
var
 x,y:integer;
 bt:Tbitmap;
begin
 bt:=Tbitmap.Create;
 bt.Width:=image1.picture.Height;
 bt.Height:=image1.picture.Width;
 // �������
 for y:=0 to bt.Height do
 for x:=0 to bt.Width do
     bt.Canvas.Pixels[x,y]:=image1.Canvas.Pixels[bt.Height-y,x];
 // �� �����
 image1.Picture.Bitmap.Assign(bt);
 bt.Free;
 fmSnimok.Color:=clgreen;
end;

procedure TfmSnimok.rightRotClick(Sender: TObject);
var
 x,y:integer;
 bt:Tbitmap;
begin
 bt:=Tbitmap.Create;
 bt.Width:=image1.picture.Height;
 bt.Height:=image1.picture.Width;
 // �������
 for y:=0 to bt.Height do
 for x:=0 to bt.Width do
     bt.Canvas.Pixels[x,y]:=image1.Canvas.Pixels[y,bt.width-x];
 // �� �����
 image1.Picture.Bitmap.Assign(bt);
 bt.Free;
 fmSnimok.Color:=clgreen;
end;

procedure TfmSnimok.DelButtonClick(Sender: TObject);
begin
 Image1.Picture:=nil;
end;

procedure TfmSnimok.BitBtn2Click(Sender: TObject);
begin
 try
  if Image1.Picture<>nil then plg.InsertImage(image1.Picture.Graphic);
  fmSnimok.close;
 except
 end;
end;

end.
