library snimok;

uses
  SysUtils,
  Classes,
  windows,
  forms,
  inifiles,
  controls,
  SNIMOK_main in 'SNIMOK_main.pas' {fmSnimok},
  api in '..\..\api.pas',
  snimok_setup in 'snimok_setup.pas' {SetupForm},
  global in 'global.pas';

{$R *.res}
var
  MainApp: TApplication;
  h1,h2,h3:string;
  path:string;

Function PluginName:widestring; forward;

// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
 PluginName:='Snimok plugin';
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
 PluginVer:='1.0';
end;

// ������������ �������� ��� �������� dll  (������������)
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
  end;
end;

procedure Done;
begin
  FreeAndNil(fmSnimok);
end;

// �������� ���������
procedure LoadSetup;
var
  fl:Tinifile;
begin
  fl:=Tinifile.Create(path+PluginName+'.ini');
  try
    h1:=fl.ReadString('hotkey','h1','WIN');
    h2:=fl.ReadString('hotkey','h2','');
    h3:=fl.ReadString('hotkey','h3','P');
  finally
    fl.free;
  end;
end;

// �������� ���������
procedure SaveSetup;
var
  fl:Tinifile;
begin
  fl:=Tinifile.Create(path+PluginName+'.ini');
  try
    fl.WriteString('hotkey','h1',h1);
    fl.WriteString('hotkey','h2',h2);
    fl.WriteString('hotkey','h3',h3);
  finally
    fl.free;
  end;
end;

// ������������� ������� ���������� ��� �������� (������������)
function Init(app:Tapplication):boolean;
begin
 result:=false;
 // �������� ���������
 if not App.MainForm.GetInterface(IPluginInterface,Plg) then
 begin
    MessageBox(0, '�� ������� �������� ������ �� ���������!', 'Error', mb_ok);
    exit;
 end;
 // ����� �������
 path:=extractfilepath(application.exename)+'\Plugins\'+PluginName+'_Data\';
 CreateDir(path);
 // ���������
 loadsetup;
 result:=true;
end;

{���������� ������� �������: CTRL/ALT/WIN+CTRL/ALT/WIN+�����/�����
 ps/���� ��������� �� �������}
Procedure GetHotKey(var hot1, hot2, hot3:widestring);
begin
 hot1:=widestring(h1);
 hot2:=widestring(h2);
 hot3:=widestring(h3);
end;

{�������� �� ������� ������� ������}
Procedure HotKey;
begin
 if fmSnimok=nil then
    fmsnimok:=TfmSnimok.Create(Application);
 GetScreen;
 MainApp.MainForm.Show;
 MainApp.MainForm.BringToFront;
 fmsnimok.Show;
 fmsnimok.BringToFront;
end;

{����� ����� �������� �������}
Procedure Setup;
begin
 if SetupForm=nil then
    SetupForm:=TSetupForm.Create(Application);
 SetupForm.Combo_snap.Text:=h1;
 SetupForm.Edit_snap.Text:=h3;
 SetupForm.ShowModal;
 if SetupForm.ModalResult=mrok then
 begin
   h1:=SetupForm.Combo_snap.Text;
   h3:=SetupForm.Edit_snap.Text;
   SaveSetup;
 end;
end;

exports
 Init,
 PluginName,
 PluginVer,
 GetHotKey,
 HotKey,
 Done,
 Setup;

begin
  DLLProc := @MyDLLProc;
end.
