library i_test;

uses
  Windows,
  SysUtils,
  Classes,
  Forms,
  Dialogs,
  extCtrls,
  api in '..\..\api.pas',
  vers in '..\..\VERS.PAS';


var
  Plg: IPluginInterface;
  // ������� �� ������ �������
  main_panel:widestring;
  main_btn:widestring;
  //



// ���������� ��� �������� (������������)
Function PluginName:widestring;
begin
 Result:='ITest';
end;

// ���������� ������ �������� (������������)
Function PluginVer:widestring;
begin
 PluginVer:='1.7';
end;

// ������������ �������� ��� �������� dll
procedure MyDLLProc(Reason: Integer);
begin
  if Reason = DLL_PROCESS_DETACH then
  begin
    ;
  end;
end;

// �������� �� ������ �� ������
Procedure pluginclick(sender:tobject);
begin
end;

// ������������� ������� ���������� ��� �������� (������������)
Function Init(app:Tapplication):boolean;
var
  rz:boolean;
begin
 rz:=false;
 try
   try
     // �������� ���������
     if not App.MainForm.GetInterface(IPluginInterface,Plg) then
     begin
        ShowMessage('�� ������� �������� ������ �� ���������!');
        result:=false;
        exit;
     end;
     // �����
     rz:=true;
   except
     rz:=false;
   end;
 finally
   result:=rz;
 end;
end;

{����� ����� �������� �������}
Procedure Setup;
begin
 showmessage('Setup click');
end;

exports
 Init,
 PluginName,
 PluginVer;
begin
  { ���-������ ���, ��� ������ ����������� �
    �������� ������������� ���������� }
  DLLProc := @MyDLLProc;
end.
