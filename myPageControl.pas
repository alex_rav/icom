unit myPageControl;

interface

uses System.Classes, Vcl.Forms, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Controls, Math, Themes, Types, Vcl.Graphics, windows,
  Winapi.Messages, Winapi.CommCtrl, System.SysUtils, Vcl.ImgList, commonlib, System.UITypes;

type
  TPageControl = class(Vcl.ComCtrls.TPageControl)
  private
    ThemesActive: Boolean;
    fCloseButtons: Boolean;
    //FColorTextTab: TColor;
    procedure  DoDraw(DC: HDC; DrawTabs: Boolean);
    procedure  DrawTab(LCanvas: TCanvas; Index: Integer); reintroduce;
    procedure setCloseButtons(value: Boolean);
  protected
    procedure PaintWindow(DC: HDC); override;
    procedure WndProc(var Message: TMessage); override;
  published
  public
    constructor Create(AOwner: TComponent); override;
    procedure  drawCloseButton(index:Integer; State: boolean);
    function GetButtonRect(Index:integer):TRect;
    function GetIndexTab(X, Y: Integer; var Index:Integer):Boolean;
    property CloseButtons:Boolean read fCloseButtons write setCloseButtons;
  end;

type
  TCustomTabControlClass = class(TCustomTabControl);

implementation

{$WARNINGS OFF}
function isThemesActive:boolean;
type
  func=function:boolean;
var
  lib: THandle;
  OSVerInfo: TOSVersionInfo;
  ver:string;
  pgetn:func;
begin
  OSVerInfo.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  GetVersionEx(OSVerInfo);
  ver := inttostr(OSVerInfo.dwMajorVersion)+'.'+inttostr(OSVerInfo.dwMinorVersion);
  if ver >= '6.2'  then
    result := true
  else if ver <= '5.0'  then
    result := false
  else
  begin
    try
      lib := SafeLoadLibrary(pchar('UxTheme.dll'));
      try
        pgetn := GetProcAddress(lib, 'IsAppThemed');
        if addr(pgetn) = nil then
          result := false
        else
          result := pgetn;
      finally
        freelibrary(lib);
      end;
    except
      result := true;
    end;
  end;
end;

constructor TPageControl.Create(AOwner: TComponent);
begin
  inherited;
  ThemesActive := isThemesActive;
end;

procedure TPageControl.setCloseButtons(value: Boolean);
begin
  fCloseButtons := value;
  Refresh;
end;

procedure TPageControl.WndProc(var Message: TMessage);
begin
  if(Message.Msg=TCM_ADJUSTRECT) then
  begin
    Inherited WndProc(Message);
    Case TAbPosition of
      tpTop :
      begin
      PRect(Message.LParam)^.Left:=0;
      PRect(Message.LParam)^.Right:=PRect(Message.LParam)^.Right+4;
      PRect(Message.LParam)^.Top:=PRect(Message.LParam)^.Top-3;
      PRect(Message.LParam)^.Bottom:=ClientHeight+1;
      end;
      tpBottom :
      begin
      PRect(Message.LParam)^.Left:=1;
      PRect(Message.LParam)^.Right:=PRect(Message.LParam)^.Right+4;
      PRect(Message.LParam)^.Bottom:=PRect(Message.LParam)^.Bottom;
      end;
    end;
  end
  else
    Inherited WndProc(Message);
end;
{$WARNINGS ON}

function TPageControl.GetButtonRect(Index:integer):TRect;
begin
  with TabRect(Index) do
  begin
    if CloseButtons then
    begin
      Result.Right:=Right-5;
      Result.Top:=Top+5;
      Result.Bottom:=Result.Top+Canvas.TextHeight('0');
      Result.Left:=Result.Right-(Result.Bottom-Result.Top);
    end
    else
    begin
      Result.Right:=0;
      Result.Top:=0;
      Result.Bottom:=0;
      Result.Left:=0;
    end;
  end;
end;

function TPageControl.GetIndexTab(X, Y: Integer; var Index:Integer):Boolean;
begin
  Result:=false;
  Index:=IndexOfTabAt(X, Y);
  if (Index>0) then  // ��� 0 ������� ��� ������
    Result:=PtInRect(GetButtonRect(Index), Point(X, Y));
end;

procedure TPageControl.drawCloseButton(index:Integer; State: boolean);
var
  rect:TRect;
begin
  if CloseButtons then
  begin
    Rect := TabRect(Index);
    rect.Right := rect.Right-4;
    rect.Left := rect.Right-12;
    rect.Top := rect.Top+4;
    rect.Bottom := rect.top+12;
    with Canvas do
    begin
      // �������� ������
      if state then
      begin
        Brush.color:=clMaroon;
        fillrect(rect);
      end
      else
      begin
        brush.color:=clSilver;
        fillrect(rect);
      end;
      // ������� ������
      Pen.Color:=IfThen(State, clWhite, clWhite);
      Pen.Width:=2;
      MoveTo(rect.Left+2, rect.Top+2);
      LineTo(rect.Right-3, rect.Bottom-3);
      MoveTo(rect.Right-3, rect.Top+2);
      LineTo(rect.Left+2, rect.Bottom-3);
    end;
  end;
end;

procedure TPageControl.DrawTab(LCanvas: TCanvas; Index: Integer);
var
  bm: vcl.Graphics.TBitmap;
  LDetails: TThemedElementDetails;
  LImageIndex: Integer;
  LThemedTab: TThemedTab;
  LIconRect: TRect;
  R, LayoutR: TRect;
  LImageW, LImageH, DxImage: Integer;
  LTextY: Integer;
  LTextColor: TColor;
  TabColor: Tcolor;
    //draw the text in the tab
    procedure DrawControlText(const S: string; var R: TRect; Flags: Cardinal);
    var
      TextFormat: TTextFormatFlags;
    begin
      LCanvas.Font       := Font;
      TextFormat         := TTextFormatFlags(Flags);
      LCanvas.Font.Color := LTextColor;
      StyleServices.DrawText(LCanvas.Handle, LDetails, S, R, TextFormat, LCanvas.Font.Color);
    end;

begin
  //get the size of tab image (icon)
  if (Images <> nil) then
  begin
    LImageW := Images.Width;
    LImageH := Images.Height;
    DxImage := 3;
  end
  else
  begin
    LImageW := 0;
    LImageH := 0;
    DxImage := 0;
  end;

  R := TabRect(Index);

  //check the left position of the tab.
  if R.Left < 0 then Exit;

  LCanvas.Font.Assign(Font);
  LayoutR := R;
  LThemedTab := ttTabDontCare;

  //Get the type of the active tab to draw
  case TabPosition of
    tpTop:
      begin
        if Index = TabIndex then
          LThemedTab := ttTabItemSelected
        else
          LThemedTab := ttTabItemNormal;
      end;
    tpBottom:
      begin
        if Index = TabIndex then
          LThemedTab := ttTabItemBothEdgeSelected
        else
          LThemedTab := ttTabItemBothEdgeNormal;
      end;
  end;

  //draw the tab
  if StyleServices.Available then
  begin
    LDetails := StyleServices.GetElementDetails(LThemedTab);//necesary for DrawControlText and draw the icon
    StyleServices.DrawElement(LCanvas.Handle, LDetails, R);
  end;
  // my draw
  if Index = TabIndex then
    Canvas.Brush.Color := shift(TPanel(Parent).color, 50)
  else
    Canvas.Brush.Color :=  shift(TPanel(Parent).color, 5);
  TabColor := Canvas.Brush.Color;
  R.Top := r.Top+1;
  R.Left:=r.Left+1;r.Right:=r.Right-1;
  Canvas.FillRect(R);
  Canvas.Pixels[r.Left-1, r.Bottom-1] := Canvas.Pixels[r.Left-1, r.Bottom-2];
  Canvas.Pixels[r.right, r.Bottom-1] := Canvas.Pixels[r.right, r.Bottom-2];
  Canvas.Pixels[r.right+1, r.Bottom-1] := Canvas.Pixels[r.right+1, r.Bottom-2];


  //get the index of the image (icon)
  if Self is TCustomTabControl then
    {$WARNINGS OFF}
    LImageIndex := TCustomTabControlClass(Self).GetImageIndex(Index)
    {$WARNINGS ON}
  else
    LImageIndex := Index;

  //draw the image
  if (Images <> nil) and (LImageIndex >= 0) and (LImageIndex < Images.Count) then
  begin
    LIconRect := LayoutR;
    case TabPosition of
      tpTop, tpBottom:
        begin
          LIconRect.Left := LIconRect.Left + DxImage;
          LIconRect.Right := LIconRect.Left + LImageW;
          LayoutR.Left := LIconRect.Right;
          LIconRect.Top := LIconRect.Top + (LIconRect.Bottom - LIconRect.Top) div 2 - LImageH div 2;
          LIconRect.Bottom := LIconRect.Bottom - DxImage;
        end;
    end;
    if StyleServices.Available then
    begin
      StyleServices.DrawIcon(LCanvas.Handle, LDetails, LIconRect, Images.Handle, LImageIndex);
      if not ThemesActive then
      begin // my draw
        bm:= Vcl.Graphics.TBitmap.create;
        bm.transparent:=true;
        if (pages[index].imageindex>=0)and(pages[index].imageindex < Images.count) then
        begin
          // transparent
          bm.Height := Images.Height;
          bm.Width := Images.Width;
          Images.draw(bm.Canvas, 0, 0, pages[index].imageindex, dsnormal, itmask);
          Images.draw(bm.Canvas, 0, 0, pages[index].imageindex, dsnormal, itimage);
          bm.TransParentColor := BM.Canvas.pixels[1,1];
        end;
        if not bm.Empty then
        begin
          bm.TransparentMode := tmAuto;
          Canvas.draw(r.left+2, r.top+(((r.bottom-r.top)-bm.height)div 2), bm);
        end;
        bm.free;
      end;
    end;
  end;

  //draw the text of the tab
  if StyleServices.Available then
  begin
    //LTextColor:=FColorTextTab;
    //DrawControlText(Tabs[Index], LayoutR, DT_VCENTER or DT_CENTER or DT_SINGLELINE  or DT_NOCLIP);
    if (Index = TabIndex) then
    begin
      if ThemesActive then
        LCanvas.Font.Color := shift(tabColor,80)
      else
        LCanvas.Font.Color := clWindow;
    end
    else
    begin
      if ThemesActive then
        LCanvas.Font.Color:= shift(tabcolor,80)
      else
        LCanvas.Font.Color := clBtnText;
    end;
    LTextY := (LayoutR.Bottom - LayoutR.Top) div 2 - LCanvas.TextHeight(Tabs[Index]) div 2;
    LCanvas.TextOut(LayoutR.Left+5, LayoutR.top+LtextY-1, Tabs[Index]);
  end;
  if Index <> 0 then
    drawCloseButton(Index, Index = TabIndex);
end;

procedure TPageControl.DoDraw(DC: HDC; DrawTabs: Boolean);
var
  Details: TThemedElementDetails;
  R: TRect;
  LIndex, SelIndex: Integer;
begin
  Details := StyleServices.GetElementDetails(ttTabItemNormal);
  SelIndex := TabIndex;
  try
    Canvas.Handle := DC;
    if DrawTabs then
      for LIndex := 0 to Tabs.Count - 1 do
        if LIndex <> SelIndex then
         DrawTab(Canvas, LIndex);

    if SelIndex < 0 then
      R := Rect(0, 0, Width, Height)
    else
    begin
      R := TabRect(SelIndex);
      R.Left := 0;
      R.Top := R.Bottom;
      R.Right := Width;
      R.Bottom := Height;
    end;

    StyleServices.DrawElement(DC, StyleServices.GetElementDetails(ttPane), R);

    if (SelIndex >= 0) and DrawTabs then
    begin
      DrawTab(Canvas, SelIndex);
    end;

    {Canvas.Brush.Color := TPanel(Parent).color;
    R := Rect(width-32, 0, Width-1, Height-1);
    Canvas.FillRect(r);}

  finally
    Canvas.Handle := 0;
  end;
end;

procedure TPageControl.PaintWindow(DC: HDC);
begin
 DoDraw(DC, True);
 //inherited;
end;

end.
