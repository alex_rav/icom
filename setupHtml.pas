unit setupHtml;

interface

uses Controls,
  Windows, Messages, SysUtils, Classes, Graphics, Forms,
  Menus, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  ClipBrd, DBCtrls, ImgList, ShellApi, SyncObjs, jpeg,
  AppEvnts, ExtDlgs,
  types, variants, strutils,

  ActiveX,
  Grids, Dialogs,
  JvAnimatedImage,
  mplayer,
  pngimage,
  icomView, language, setting, icomchannels, icomuserbox,
  psAPI,

  Vcl.OleCtrls;

procedure getSetup;
procedure DelAv(browser:TIcomViewer);
procedure LoadAv(browser:TIcomViewer);
procedure saveSetup(browser:TIcomViewer);
procedure selectdir(browser:TIcomViewer);
procedure selectColorFont(browser:TIcomViewer);
procedure selectColorSysFont(browser:TIcomViewer);
procedure selectColorWin(browser:TIcomViewer);
procedure fontb(browser:TIcomViewer);
procedure fonti(browser:TIcomViewer);

implementation

uses MAIN, Global, htmllib, Vcl.FileCtrl;

procedure fontb(browser:TIcomViewer);
var
  b,i:Boolean;
begin
  b := (fsBold in Setup.myFont.Style);
  i := (fsItalic in Setup.myFont.Style);
  b:= not b;  // toogle
  Setup.myFont.Style := [];
  if b then
    Setup.myFont.Style := [fsBold];
  if i then
    Setup.myFont.Style := Setup.myFont.Style + [fsItalic];
  Browser.SetButton('sWideFontBtn',(fsBold in Setup.myFont.Style));
end;

procedure fonti(browser:TIcomViewer);
var
  b,i:Boolean;
begin
  b := (fsBold in Setup.myFont.Style);
  i := (fsItalic in Setup.myFont.Style);
  i:= not i;  // toogle
  Setup.myFont.Style := [];
  if b then
    Setup.myFont.Style := [fsBold];
  if i then
    Setup.myFont.Style := Setup.myFont.Style + [fsItalic];
  Browser.SetButton('sItalicFontBtn',(fsItalic in Setup.myFont.Style));
end;

function copyAvatar():string;
begin
  result := htmldir + 'av'+inttostr(GetTickCount)+'.jpg';
  copyfile(PChar(Path + myavatar), PChar(result), false);
end;

procedure DelAv(browser:TIcomViewer);
var
  newAv: string;
begin
  try
    mainForm.DefaultAvatar.picture.SaveToFile(Path + myavatar);
    newAv := copyAvatar();
    Browser.SetImageSrc('av_image', 'file:///'+newAv);
  except
  end;
end;

procedure LoadAv(browser:TIcomViewer);
var
  img: TJpegImage;
  h, w: Integer;
  kf: real;
  TmpBmp: TBitmap;
  ARect: TRect;
  av:string;
begin
  mainForm.OpenPictureDialog1.filter := 'jpeg|*.jpg; *.jpeg';
  mainForm.OpenPictureDialog1.FilterIndex := 0;
  if mainForm.OpenPictureDialog1.Execute then
  begin
    img := TJpegImage.Create;
    try
      if fileexists(mainForm.OpenPictureDialog1.FileName) then
        img.LoadFromFile(mainForm.OpenPictureDialog1.FileName)
      else
        exit;
      // �������� �� 100�100
      if (img.Height > 100) or (img.width > 100) then
      begin
        h := img.Height;
        w := img.width;
        if h > w then
          kf := h / 100
        else
          kf := w / 100;
        h := trunc(h / kf);
        w := trunc(w / kf);
        TmpBmp := TBitmap.Create;
        TmpBmp.Height := h;
        TmpBmp.width := w;
        ARect := Rect(0, 0, w, h);
        TmpBmp.Canvas.StretchDraw(ARect, img);
        img.Assign(TmpBmp);
        img.CompressionQuality := 90;
        img.Compress;
        FreeAndNil(TmpBmp);
      end;
      img.SaveToFile(Path + myavatar);
      av := copyAvatar;
      Browser.SetImageSrc('av_image', 'file:///'+Av);
    finally
      FreeAndNil(img);
    end;
  end;
end;

procedure selectdir(browser:TIcomViewer);
var
  dir:string;
begin
  dir := Setup.myPath;
  if SelectDirectory('','', dir, [], nil)  then
    browser.SetFieldValue(0, 'edit_path', dir);
end;

procedure selectColorFont(browser:TIcomViewer);
begin
  mainForm.ColorDialog1.color := setup.myFont.Color;
  if mainform.ColorDialog1.Execute then
  begin
    browser.SetBGColor('PanelFontColor2', HTMLColor(mainForm.ColorDialog1.color));
  end;
end;

procedure selectColorSysFont(browser:TIcomViewer);
begin
  mainForm.ColorDialog1.color := setup.sysfont.Color;
  if mainform.ColorDialog1.Execute then
  begin
    browser.SetBGColor('PanelSysColor', HTMLColor(mainForm.ColorDialog1.color));
  end;
end;

procedure selectColorWin(browser:TIcomViewer);
begin
  mainForm.ColorDialog1.color := setup.wincolor;
  if mainform.ColorDialog1.Execute then
  begin
    browser.SetBGColor('MemoColor', HTMLColor(mainForm.ColorDialog1.color));
  end;
end;

function createFontList(id:string):string;
var
  i:Integer;
begin
  Result := '<select id='+id+'>';
  for i:=0 to mainForm.FontComboBox1.Items.Count-1 do
     Result := Result + '<option value="'+inttostr(i)+'">'+mainForm.FontComboBox1.Items[i]+'</option>';
  Result := Result + '</select>';
end;

function createLangList(id:string):string;
var
  i:Integer;
  sr:TSearchRec;
begin
  Result := '<select id='+id+'>';
  i := 0;
  If findFirst(LangDir + '*.lng', faAnyFile, sr) = 0 then
  begin
    repeat
      Result := Result + '<option value="'+inttostr(i)+'">'+sr.name+'</option>';
      i := i+1;
    until FindNext(sr) <> 0;
    FindClose(sr);
  end
  else
  begin
    Result := Result + '<option value="'+inttostr(i)+'">'+default_lang+'</option>';
  end;
  Result := Result + '</select>';
end;

function FindSizeindex(size:integer):integer;
const
  spis:array[0..10] of integer = (6,8,10,11,12,14,16,18,20,24,30);
var
  i:integer;
begin
  result := 4;
  for i := 0 to length(spis)-1 do
    if size >= spis[i] then
    begin
      result := i;
      break;
    end
end;

function FindFontIndex(fontName:string):integer;
var
  spis:TStringList;
  i:integer;
begin
  spis := TStringList.Create;
  spis.Assign(mainForm.FontComboBox1.Items);
  result := 0;
  for i := 0 to spis.Count-1 do
    if fontName = spis[i] then
    begin
      result := i;
      break;
    end
end;

function FindFontName(index:integer):string;
var
  spis:TStringList;
begin
  spis := TStringList.Create;
  try
    spis.Assign(mainForm.FontComboBox1.Items);
    result := spis[index];
  except
    Result := spis[0];
  end;
end;

function FindLangIndex(lang:string):integer;
var
  i:integer;
  spis:TStringList;
  sr: TSearchRec;
begin
  spis := TStringList.Create;
  If findFirst(LangDir + '*.lng', faAnyFile, sr) = 0 then
  begin
    repeat
      spis.Add(sr.name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
  result := 0;
  for i := 0 to spis.Count-1 do
    if Pos(copy(lang,1,2), spis[i]) <> 0 then
    begin
      result := i;
      break;
    end
end;

function FindLangName(index:integer):string;
var
  spis:TStringList;
  sr: TSearchRec;
begin
  spis := TStringList.Create;
  If findFirst(LangDir + '*.lng', faAnyFile, sr) = 0 then
  begin
    repeat
      spis.Add(sr.name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
  result := spis[index];
end;

procedure getSetup;
var
  chname: string;
  sl: TStringList;
  elName:string;
  elValue: string;
  ps1,ps2:integer;
  ss: string;
  i: integer;
  browser: TIcomViewer;
  av: string;
begin
  try
    chname := lang.Get('setup');
    browser := Channels[ChName].Browser;
    // ���� (label)
    sl := lang.getList(Setup.icomlang);
    for i := 0 to sl.Count-1 do
    begin
      if (pos('setupform.', sl.Strings[i]) <> 0)and(pos('.caption', sl.Strings[i]) <> 0) then
      begin
        ss := sl.Strings[i];
        ps1 := Pos('.', ss);
        ps2 := Posex('.', ss, ps1+1);
        elName := Copy(ss, ps1+1, ps2-ps1-1);
        elValue := copy(ss,pos('=',ss)+1);
        if elName > '' then
          Browser.SetElementById(elName, elValue);
      end;
    end;
    // radio combo
    for I := 0 to 10 do
    begin
      Browser.SetElementById('checkicon'+IntToStr(i), sl.Values['setupform.CheckIcon.items'+inttostr(i)]);
      Browser.SetElementById('comboicons'+IntToStr(i)+'_', sl.Values['setupform.comboicons.items'+inttostr(i)]);
      Browser.SetElementById('RadioNot'+IntToStr(i), sl.Values['setupform.radionot'+inttostr(i)+'.caption']);
      Browser.SetElementById('ComboZoom'+IntToStr(i)+'_', sl.Values['setupform.combozoom.items'+inttostr(i)]);
      Browser.SetElementById('ComboDel'+IntToStr(i)+'_', sl.Values['setupform.combodel.items'+inttostr(i)]);
    end;
    // button
    Browser.SetFieldValue(0, 'loadavbtn', sl.Values['setupform.LoadAvBtn.caption']);
    Browser.SetFieldValue(0, 'delavbtn', sl.Values['setupform.delAvBtn.caption']);
    Browser.SetFieldValue(0, 'setupsavebtn', sl.Values['save']);
    Browser.SetFieldValue(0, 'setupcancelbtn', sl.Values['cancel']);
    // ������ ��������
    Browser.SetElementById('personal_', sl.Values['setupform.PageControl2.items0']);
    Browser.SetElementById('interface_', sl.Values['setupform.PageControl2.items1']);
    Browser.SetElementById('common_', sl.Values['setupform.PageControl2.items2']);
    Browser.SetElementById('font_', sl.Values['setupform.PageControl2.items4']);
    Browser.SetElementById('tray_', sl.Values['setupform.PageControl2.items5']);
    Browser.SetElementById('sound_', sl.Values['setupform.PageControl2.items6']);
    Browser.SetElementById('lang_', sl.Values['setupform.PageControl2.items7']);
    Browser.SetElementById('traf_', sl.Values['setupform.PageControl2.items8']);
    Browser.SetElementById('other_', sl.Values['setupform.PageControl2.items9']);
    Browser.SetElementById('protect_', sl.Values['setupform.PageControl2.items10']);

    // ��������
    Browser.SetImageSrc('ipers', 'file:///'+CacheDir+'pers.gif');
    Browser.SetImageSrc('iint', 'file:///'+CacheDir+'int.gif');
    Browser.SetImageSrc('icommon', 'file:///'+CacheDir+'common.gif');
    Browser.SetImageSrc('ifonts', 'file:///'+CacheDir+'fonts.gif');
    Browser.SetImageSrc('itray', 'file:///'+CacheDir+'tray.gif');
    Browser.SetImageSrc('isound', 'file:///'+CacheDir+'sound.gif');
    Browser.SetImageSrc('ilang', 'file:///'+CacheDir+'lang.gif');
    Browser.SetImageSrc('itraf', 'file:///'+CacheDir+'traf.gif');
    Browser.SetImageSrc('iother', 'file:///'+CacheDir+'other.gif');
    Browser.SetImageSrc('iprotect', 'file:///'+CacheDir+'protect.gif');
    // ������
    av := copyAvatar;
    Browser.SetImageSrc('av_image', 'file:///'+av);
    Browser.SetFieldValue(0, 'edit_nick', setup.MyName);
    Browser.SetFieldValue(0, 'edit_dr', setup.dr);
    Browser.SetFieldValue(0, 'mgradio'+IntToStr(setup.male)+'_', 'TRUE');
    // ���������
    Browser.SetFieldValue(0, 'checkIcon'+IntToStr(setup.userboxmode)+'_', 'TRUE');
    Browser.SetFieldValue(0, 'CheckLine_', BoolToStr(setup.drawlines, true));
    Browser.SetFieldValue(0, 'CheckInfo_', BoolToStr(setup.showinfo, true));
    Browser.SetFieldValue(0, 'CheckOffLine_', BoolToStr(setup.ShowOffline, true));
    Browser.SetFieldValue(0, 'CheckSortMode_', BoolToStr(setup.userboxsort, true));
    Browser.SetFieldValue(0, 'CheckOffLineEnd_', BoolToStr(setup.OfflineEnd, true));
    Browser.SetFieldValue(0, 'CheckQuickSmiles_', BoolToStr(setup.quickSmiles, true));
    Browser.SetFieldValue(0, 'CheckActPage_', BoolToStr(setup.actpage, true));
    Browser.SetFieldValue(0, 'CheckRGB_', BoolToStr(setup.showrgb, true));
    Browser.SetFieldValue(0, 'CheckPagesOneLine_', BoolToStr(setup.MultirowPages, true));
    Browser.SetBGColor('memocolor', htmlColor(setup.WinColor));
    // �����
    if not Setup.entermode2 then
      Browser.SetFieldValue(0, 'RadioNot0_', 'TRUE')
    else
      Browser.SetFieldValue(0, 'RadioNot1_', 'TRUE');
    Browser.SetFieldValue(0, 'Combo_snap', setup.hot1);
    Browser.SetFieldValue(0, 'edit_snap', setup.hot3);
    Browser.SetFieldValue(0, 'CheckDelete_', BoolToStr(setup.confirmdelete, true));
    Browser.SetFieldValue(0, 'CheckDelMsg_', BoolToStr(setup.confirmdelmsg, true));
    Browser.SetFieldValue(0, 'CheckClosePage_', BoolToStr(setup.confirmclosepage, true));
    Browser.SetFieldValue(0, 'CheckSmiles_', BoolToStr(setup.closeSmiles, true));

    // ������
    Browser.SetElementByIdOuter('Combo_font_', createFontList('Combo_font_'));
    browser.SetFieldValue(0, 'Combo_font_', inttostr(findFontIndex(Setup.myFont.Name)));
    Browser.SetBGColor('PanelFontColor2', htmlColor(setup.myFont.Color));
    Browser.SetButton('sWideFontBtn',(fsBold in Setup.myFont.Style));
    Browser.SetButton('sItalicFontBtn',(fsItalic in Setup.myFont.Style));
    Browser.SetFieldValue(0, 'ComboBox_size', IntToStr(setup.myFont.Size));

    Browser.SetElementByIdOuter('sys_font', createFontList('sys_font'));
    browser.SetFieldValue(0, 'sys_font', inttostr(findFontIndex(Setup.sysfont.Name)));
    Browser.SetBGColor('PanelSysColor', htmlColor(setup.sysFont.Color));
    Browser.SetFieldValue(0, 'sys_font_size', IntToStr(setup.sysFont.Size));

    // ����
    Browser.SetFieldValue(0, 'Check_trayonoff_', BoolToStr(setup.trayonoff, true));
    Browser.SetFieldValue(0, 'Check_ballon_', BoolToStr(setup.showballon, true));
    // �����
    Browser.SetFieldValue(0, 'CheckSound_', BoolToStr(setup.soundon, true));

    // ����
    Browser.SetElementByIdOuter('ComboLang2_', createLangList('ComboLang2_'));
    Browser.SetFieldValue(0, 'ComboLang2_', IntToStr(findLangIndex(setup.icomlang)));

    // ������
    Browser.SetFieldValue(0, 'ComboZoom_', IntToStr(setup.zoomMode));
    Browser.SetFieldValue(0, 'ComboDel_', IntToStr(setup.delbtn));
    Browser.SetFieldValue(0, 'CheckBtnQuote_', BoolToStr(setup.quotebtn, true));
    Browser.SetFieldValue(0, 'CheckFree_', BoolToStr(setup.autofree, true));
    Browser.SetFieldValue(0, 'CheckCollapse_', BoolToStr(setup.Collapselong, true));
    Browser.SetFieldValue(0, 'CheckMsgBg_', BoolToStr(setup.message_bg, true));

    // ������
    browser.SetFieldValue(0, 'edit_path', Setup.myPath);
    browser.SetFieldValue(0, 'edit_port', IntToStr(Setup.IcomPort));
    // ������
    Browser.SetFieldValue(0, 'CheckExit_', BoolToStr(setup.checkexit, true));
    //Browser.SetFieldValue(0, 'CheckTraf_', BoolToStr(setup.checktraf, true));
    Browser.SetFieldValue(0, 'CheckAutoConnect_', BoolToStr(setup.autoconnect, true));
    //Browser.SetFieldValue(0, 'CheckSaveTraf_', BoolToStr(setup.saveTraf, true));
    Browser.SetFieldValue(0, 'CheckStopTray_', BoolToStr(setup.stoptray, true));
    browser.SetFieldValue(0, 'edit_exit', IntToStr(Setup.idleexit));
    //browser.SetFieldValue(0, 'edit_traf', IntToStr(Setup.traflimit));
    browser.SetFieldValue(0, 'edit_idle', IntToStr(Setup.idlemax));
  except
    on E: Exception do
      ;
  end
end;

procedure saveSetup(browser:TIcomViewer);
var
  icom_lang_old: string;
  green_old, blue_old, red_old, setup_old: string;
  old_width :integer;
  old_bg:Tcolor;
  procedure renameRGB;
  var
    i: Integer;
  begin
      for i := 0 to mainForm.PageControl1.PageCount-1 do
      begin
         if lang.isGreen(Trim(mainForm.PageControl1.Pages[i].Caption)) then
           mainForm.PageControl1.Pages[i].Caption := lang.get('green')+'      '
         else if lang.isBlue(Trim(mainForm.PageControl1.Pages[i].Caption)) then
           mainForm.PageControl1.Pages[i].Caption := lang.get('blue')+'      '
         else if lang.isRed(Trim(mainForm.PageControl1.Pages[i].Caption)) then
           mainForm.PageControl1.Pages[i].Caption := lang.get('red')+'      ';
      end;
      for i:= 0 to Channels.Count-1 do
      begin
         if lang.isGreen(Channels.get(i).Name) then
           Channels.get(i).Name := lang.get('green')
         else if lang.isBlue(Channels.get(i).Name) then
           Channels.get(i).Name := lang.get('blue')
         else if lang.isRed(Channels.get(i).Name) then
           Channels.get(i).Name := lang.get('red');
      end;
  end;
begin
  old_width := mainform.panel_user_box.width;
  old_bg := setup.WinColor;
  try
    main.inUpdate := true;
    icom_lang_old := Setup.icomlang;
    green_old := lang.get('green');
    blue_old := lang.get('blue');
    red_old := lang.get('red');
    setup_old := lang.get('setup');
    // ������
    setup.MyName := Browser.GetFieldValue(0, 'edit_nick');
    setup.dr := Browser.GetFieldValue(0, 'edit_dr');
    if strtobool(Browser.GetFieldValue(0, 'mgradio0_')) = TRUE then
      setup.male := 0
    else
      Setup.male := 1;
    // ���������
    if strtoBool(Browser.GetFieldValue(0, 'checkIcon0_')) = TRUE then
      setup.userboxmode := 0
    else if strtoBool(Browser.GetFieldValue(0, 'checkIcon1_')) = TRUE then
      setup.userboxmode := 1
    else if strtoBool(Browser.GetFieldValue(0, 'checkIcon2_')) = TRUE then
      setup.userboxmode := 2
    else if strtoBool(Browser.GetFieldValue(0, 'checkIcon3_')) = TRUE then
      setup.userboxmode := 3;
    setup.drawlines := StrToBool(Browser.GetFieldValue(0, 'CheckLine_'));
    setup.showinfo := StrToBool(Browser.GetFieldValue(0, 'CheckInfo_'));
    setup.ShowOffline := StrToBool(Browser.GetFieldValue(0, 'CheckOffLine_'));
    setup.userboxsort := StrToBool(Browser.GetFieldValue(0, 'CheckSortMode_'));
    setup.OfflineEnd := StrToBool(Browser.GetFieldValue(0, 'CheckOffLineEnd_'));
    setup.quickSmiles := StrToBool(Browser.GetFieldValue(0, 'CheckQuickSmiles_'));
    //setup.channelicons := StrToInt(Browser.GetFieldValue(0, 'comboicons_'));
    setup.actpage := StrToBool(Browser.GetFieldValue(0, 'CheckActPage_'));
    setup.showrgb := StrToBool(Browser.GetFieldValue(0, 'CheckRGB_'));
    setup.MultirowPages := StrToBool(Browser.GetFieldValue(0, 'CheckPagesOneLine_'));
    Setup.WinColor := HtmlColor2Color(Browser.GetBGColor('memocolor'));
    // �����
    Setup.entermode2 := StrToBool(Browser.GetFieldValue(0, 'RadioNot1_'));
    setup.hot1 := Browser.GetFieldValue(0, 'Combo_snap', 0);
    setup.hot3 := Browser.GetFieldValue(0, 'edit_snap', 0);
    setup.confirmdelete := StrToBool(Browser.GetFieldValue(0, 'CheckDelete_'));
    setup.confirmdelmsg := StrToBool(Browser.GetFieldValue(0, 'CheckDelMsg_'));
    setup.confirmclosepage := StrToBool(Browser.GetFieldValue(0, 'CheckClosePage_'));       ///////
    setup.closeSmiles := StrToBool(Browser.GetFieldValue(0, 'CheckSmiles_'));

    // ������
    Setup.myFont.Name := FindFontName(StrToInt(browser.GetFieldValue(0, 'Combo_font_')));
    setup.myFont.Color := HtmlColor2Color(Browser.GetBGColor('PanelFontColor2'));
    Setup.myFont.Style := [];
    if Browser.GetButton('sWideFontBtn') then
      Setup.myFont.Style := [fsBold];
    if Browser.GetButton('sItalicFontBtn') then
      Setup.myFont.Style := Setup.myFont.Style + [fsItalic];
    setup.myFont.Size := StrToInt(Browser.GetFieldValue(0, 'ComboBox_size'));

    Setup.sysfont.Name := FindFontName(StrToInt(browser.GetFieldValue(0, 'sys_font')));
    setup.sysFont.Color := htmlColor2color(Browser.getBGColor('PanelSysColor'));
    setup.sysFont.Size := strtoint(Browser.GetFieldValue(0, 'sys_font_size'));

    // ����
    setup.trayonoff := StrToBool(Browser.GetFieldValue(0, 'Check_trayonoff_'));
    setup.showballon := StrToBool(Browser.GetFieldValue(0, 'Check_ballon_'));
    // �����
    setup.soundon := StrToBool(Browser.GetFieldValue(0, 'CheckSound_'));

    // ����
    setup.icomlang := findLangName(strtoint(Browser.GetFieldValue(0, 'ComboLang2_')));

    // ������
    setup.zoomMode := StrToInt(Browser.GetFieldValue(0, 'ComboZoom_'));
    setup.delbtn := StrToInt(Browser.GetFieldValue(0, 'ComboDel_'));
    setup.quotebtn := StrToBool(Browser.GetFieldValue(0, 'CheckBtnQuote_'));
    setup.autofree := StrToBool(Browser.GetFieldValue(0, 'CheckFree_'));
    setup.Collapselong := StrToBool(Browser.GetFieldValue(0, 'CheckCollapse_'));
    setup.message_bg := StrToBool(Browser.GetFieldValue(0, 'CheckMsgBg_'));

    // ������
    Setup.myPath := browser.GetFieldValue(0, 'edit_path');
    Setup.IcomPort := StrToInt(browser.GetFieldValue(0, 'edit_port'));
    // ������
    setup.checkexit := StrToBool(Browser.GetFieldValue(0, 'CheckExit_'));
    //setup.checktraf := StrToBool(Browser.GetFieldValue(0, 'CheckTraf_'));
    setup.autoconnect := StrToBool(Browser.GetFieldValue(0, 'CheckAutoConnect_'));
    //setup.saveTraf := StrToBool(Browser.GetFieldValue(0, 'CheckSaveTraf_'));
    setup.stoptray := StrToBool(Browser.GetFieldValue(0, 'CheckStopTray_'));
    Setup.idleexit := StrToInt(browser.GetFieldValue(0, 'edit_exit'));
    //Setup.traflimit := StrToInt(browser.GetFieldValue(0, 'edit_traf'));
    Setup.idlemax := StrToInt(browser.GetFieldValue(0, 'edit_idle'));

    // ��������
    Setup.SaveSetup;
    //main.start_time := now; // ���� ����� �����������
    Setup.LoadSetupNoUsers;
    mainform.ApplySetup;
    // �������� ���� - ���������� ������
    if icom_lang_old <> Setup.icomlang then
    begin
      begin  // order!
        lang.LoadLang(Setup.icomlang);
        renameRGB;
        //GetSetup;
      end;
    end;
  finally
    if (old_bg <> setup.WinColor)or(icom_lang_old <> Setup.icomlang) then
    begin
      mainForm.SetupBtnClick(nil);
      user_box.UpdateBox(true);
    end;
    mainform.MySetFocus(mainform.memoin);
    mainform.panel_user_box.width := old_width;
    mainform.Timer1sekTimer(nil);
    inUpdate := false;
  end;
end;

end.
