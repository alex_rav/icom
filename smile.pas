unit smile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RVStyle, extctrls, Grids, stdctrls, ComCtrls,
  JvAnimatedImage,
  JvGIFCtrl, Menus, ExtDlgs, icomView, desktop, reslib;

type
  TSmileForm = class(TForm)
    AddTrafMenu: TPopupMenu;
    AddPictMenu: TMenuItem;
    DeletePictMenu: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    SmilePanel: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CreateMainViewHTML;
    procedure CreateAniView;
    procedure CreateAddView;
    procedure WebBrowser0BeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure AddPictMenuClick(Sender: TObject);
    procedure DelImage(fname: string);
    procedure DeletePictMenuClick(Sender: TObject);
    procedure CreateFullViewHTML;
  private
    { Private declarations }
    htmlviewer0: TIcomViewer;
  public
    { Public declarations }
  end;

var
  SmileForm: TSmileForm;

implementation

uses main, commonLib, Global, ulog, wbpopup, htmllib;

var
  temp: TstringList;

{$R *.DFM}

procedure TSmileForm.CreateFullViewHTML;
begin
  reslib.extractHtmlSmile;
  temp.Clear;
  temp.LoadFromFile(HTMLdir + 'Smiles\smiles.html');
  CreateMainViewHTML;
  CreateAniView;
  CreateAddView;
  temp.add( '</body></html>');

  htmlviewer0 := TIcomViewer.create(SmilePanel);
  htmlviewer0.Visible := False;
  htmlviewer0.Align := alclient;
  htmlviewer0.OnBeforeNavigate2 := WebBrowser0BeforeNavigate2;
  temp.Text := StringReplace(temp.Text, '<body', '<body style="background-color: ' + HTMLColor(Setup.WinColor) + '"', []);
  htmlviewer0.LoadFromString(temp.Text);
  htmlviewer0.PopupMenu := AddTrafMenu;
end;

procedure TSmileForm.CreateMainViewHTML;
var
  i: integer;
  sr: TSearchRec;
  sl: TstringList;
  ht: TStringList;
  ext: string;
  dir: string;
begin
  dir := HTMLdir + '\Smiles\';
  sl := TstringList.create;
  if FindFirst(dir + 'smi*.gif', faAnyfile, sr) = 0 then
    repeat
      if (sr.Name = '.') or (sr.Name = '..') then
        continue;
      ext := uppercase(extractfileext(sr.Name));
      if (ext = '.GIF') then
        sl.Add(sr.Name);
    until (findnext(sr) <> 0);
  findclose(sr);
  sl.Sort;

  ht := TStringList.Create;
  ht.add('<div id=t1 width=80%>');
  for i := 0 to sl.Count-1 do
    ht.add( ' <a href="' + LNK_FILE + dir + sl.Strings[i] + '">' + '<img border="0" src="' + dir + sl.Strings[i] + '">' + '</a> &nbsp;');
  ht.add('</div>');
  temp.AddStrings(ht);
  sl.Free;
  ht.Free;
end;

procedure TSmileForm.CreateAniView;
var
  i: integer;
  sl: TstringList;
  ht: TstringList;
  sr: TSearchRec;
  ext: string;
  dir: string;
const
  w_max = 60;
begin
  dir := HTMLdir + '\Smiles\';
  sl := TstringList.create;
  if FindFirst(HTMLdir + '\Smiles\ani*.*', faAnyfile, sr) = 0 then
    repeat
      if (sr.Name = '.') or (sr.Name = '..') then
        continue;
      ext := uppercase(extractfileext(sr.Name));
      if (ext = '.GIF') then
        sl.Add(sr.Name);
    until (findnext(sr) <> 0);
  findclose(sr);
  sl.Sort;

  ht := TStringList.Create;
  ht.add( '<div id=t2 style="display: none;" width=80%>');
  for i := 0 to sl.Count-1 do
    ht.add( ' <a href="' + LNK_FILE + dir + sl.Strings[i] + '">' + '<img style="border: 0px solid;" src="' + dir + sl.Strings[i] + '">' + '</a> &nbsp;');
  ht.add( '</div>');
  temp.AddStrings(ht);
  sl.Free;
  ht.Free;
end;

procedure TSmileForm.CreateAddView;
var
  i: integer;
  sr: TSearchRec;
  sl: TstringList;
  ht: TstringList;
  ext: string;
  w, h: Word;
  w_text: string;
  dir: string;
const
  w_max = 100;
begin
  dir := HTMLdir + 'AddSmiles\';
  sl := TstringList.create;
  if FindFirst(dir + '*.*', faAnyfile, sr) = 0 then
    repeat
      if (sr.Name = '.') or (sr.Name = '..') then
        continue;
      ext := uppercase(extractfileext(sr.Name));
      if (ext = '.GIF') or (ext = '.JPG') or (ext = '.JPEG') or (ext = '.PNG') then
        sl.Add(sr.Name);
    until (findnext(sr) <> 0);
  findclose(sr);
  sl.Sort;

  ht := TStringList.Create;
  ht.add('<div id=t3 style="display: none;" width=80%>');
  for i := 0 to sl.Count - 1 do
  begin
    GetImageSize(dir + sl.Strings[i], w, h);
    if (w > w_max) then
      w_text := 'width: ' + inttostr(w_max) + 'px;'
    else
      w_text := '';
    ht.add( ' <a href="' + LNK_FILE + dir + sl.Strings[i] + '">' + '<img style="' + w_text + ' border: 0px;" src="' + dir + sl.Strings[i] + '">' + '</a> &nbsp;');
  end;
  ht.Add('</div>');
  temp.AddStrings(ht);
  sl.Free;
  ht.Free;
end;

procedure TSmileForm.FormCreate(Sender: TObject);
begin
  Self.DoubleBuffered := True;
  self.color := Setup.WinColor;
  SmilePanel.color := Setup.WinColor;
  SmileForm.width := 600;
  SmileForm.height := 400;
  temp := TStringList.Create;
end;

procedure TSmileForm.DeletePictMenuClick(Sender: TObject);
begin
  if not fileexists(copy(wbPopup.href,9)) then
    exit;
  delimage(copy(wbPopup.href,9));
end;

procedure TSmileForm.DelImage(fname: string);
begin
  if MessageDlg('������� ' + fname + ' ?', mtWarning, [mbYes, mbNo], 0) <> mrYes then
    exit;
  if fileexists(fname) then
  begin
    deletefile(fname);
    CreateAddView;
  end;
end;

procedure TSmileForm.FormActivate(Sender: TObject);
begin
  SmileForm.left := mainform.Left + mainform.SmileBtn3.Left +  mainform.SmileBtn3.Width - (self.Width div 2) ;
  if SmileForm.left < 0 then
    SmileForm.left := 0;
  SmileForm.top := MainForm.top + MainForm.MemoinPanel.top - Self.Height + WindowTitle;
  if SmileForm.Top < 0 then
    SmileForm.Top := 0;
  CreateFullViewHTML;
  Self.WindowState := wsNormal;
end;

procedure TSmileForm.WebBrowser0BeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);
var
  url2: string;
  tempGif: TjvGIFAnimator;
  nm: string;
  ext: string;
  tempImg: TImage;
begin
  url2 := urldecode(URL);
  url2 := dellast(url2);
  url2 := Copy(url2, Pos('//', url2)+2);
  if ((uppercase(extractfilepath(url2))=uppercase(htmldir+'SMILES\'))and(uppercase(extractfileext(url2))='.HTML'))
   or((uppercase(extractfilepath(url2))=uppercase(htmldir+'ADDSMILES\'))and(uppercase(extractfileext(url2))='.HTML')) then
  begin
    cancel:=false;
    exit;
  end;
  try
    if (uppercase(extractfilepath(url2))=uppercase(htmldir+'ADDSMILES\')) then
    begin
      if copy(url2,1,6)='del://' then
        delImage(copy(url2,7))
      else
      begin
        try
          MainForm.InsertFileImage(url2);
        finally
          cancel := true;
          if Setup.closeSmiles then
            close;
        end;
      end;
    end
    else
    begin
      ext := uppercase(extractfileext(url2));
      if ext = '.GIF' then
      begin
        tempGif := TjvGIFAnimator.create(nil);
        tempGif.Image.LoadFromFile(url2);
        tempGif.Animate := true;
        tempGif.AutoSize := true;
        nm := copy(extractfilename(url2), 1, pos('.GIF', extractfilename(uppercase(url2))) - 1);
        MainForm.memoin.InsertControl(ansistring(nm), tempGif, rvvaBaseline);
        if Setup.closeSmiles then
          close;
      end
      else if (ext = '.PNG') or (ext = '.JPG') then
      begin
        tempImg := TImage.create(nil);
        tempImg.Picture.LoadFromFile(url2);
        tempImg.Transparent := true;
        tempImg.AutoSize := true;
        tempImg.visible := False;
        nm := copy(extractfilename(url2), 1, pos('.PNG', extractfilename(uppercase(url2))) - 1);
        MainForm.memoin.InsertControl(ansistring(nm), tempImg, rvvaBaseline);
        if Setup.closeSmiles then
          close;
      end;
    end;
    MainForm.memoin.reFormat;
  finally
    Cancel:=true;
  end;
end;

procedure TSmileForm.AddPictMenuClick(Sender: TObject);
var
  file_name: string;
begin
  try // opendialog ����� �������� �� ��������� ������
    if (pos(path, OpenPictureDialog1.FileName) <> 0) or (pos(path, OpenPictureDialog1.Initialdir) <> 0) or (OpenPictureDialog1.FileName = '') or
      (OpenPictureDialog1.Initialdir = '') then
    begin
      OpenPictureDialog1.FileName := '*.*';
      OpenPictureDialog1.Initialdir := path;
    end;
    OpenPictureDialog1.filter := 'All |*.gif;*.png;*.jpg;*.jpeg';
    OpenPictureDialog1.FilterIndex := 0;
    if not OpenPictureDialog1.Execute then
      exit;
    file_name := OpenPictureDialog1.FileName;
    if fileexists(file_name) then
    begin
      mycopyfile(file_name, HTMLdir + 'addsmiles\' + extractfilename(file_name));
      CreateAddView;
    end;
  except
    on E: Exception do
      log('addsmile ' + E.Message);
  end;
end;

end.
